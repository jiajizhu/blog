# 基于 wide&deep 模型的广告粗排系统
## 前言
粉丝头条广告处理候选时，分为召回、过滤、排序多个阶段，排序阶段使用 DNN 深度模型，由于模型复杂，受制于超时时间限制（低于 50 ms），单次调用输入的候选不能太多（不超过100个），但是实际上召回阶段返回的候选往往有上千个，这就需要引入一个粗排系统，实现多级排序。

## 粗排介绍
粗排在召回和精排之间，一般需要从上万个广告集合中选择出几百个符合后链路目标的候选广告，并送给后面的精排模块。粗排有很严格的时间要求，一般需要在10～20ms内完成打分。

粗排在工业界的发展历程可以分成下面几个阶段：
- 第一代粗排是静态质量分，一般基于广告的历史平均 CTR，只使用了广告侧的信息，表达能力有限。
- 第二代粗排是以LR为代表的早期机器学习模型，模型结构比较简单，有一定的个性化表达能力，可以在线更新和服务。
- 第三代粗排模型，是基于向量内积的深度模型，比如 DSSM 双塔模型。

粉丝头条广告的召回阶段采用了 DSSM 双塔模型，所以在粗排阶段我们使用 wide&deep 模型来实现，wide&deep 模型非常适合应用在推荐和排序场景中，为满足 20 ms 时延要求，模型设计的也不能太复杂。

## wide&deep 介绍
wide&deep 是 TensorFlow 在 2016 年发布的一类用于分类和回归的模型，并应用到了 Google Play 的应用推荐中。

wide&deep 模型核心思想是结合线性模型的记忆能力（memorization）和 DNN 模型的泛化能力（generalization），在训练过程中同时优化 2 个模型的参数，从而达到整体模型的预测能力最优。

### 网络结构
可以认为 wide&deep = LR + DNN。

#### wide
采用 LR 模型，输入为 one-hot 编码的离散特征，需要通过特征交叉来提高模型的表达能力，具备记忆性和可解释性。

LR 模型学习不到从未出现过的特征组合，不具备泛化能力，特征交叉给特征工程带来了很大的工作量。

#### deep
采用 DNN 模型，输入为连续特征和 embeding 特征，模型训练过程中 embedding 特征也会自动训练，每个特征在 embedding 中都对应一个向量，向量与向量之间体现了对应特征的远近关系。这样针对样本中从未出现的特征组合，也可以推理出来，具备泛化能力。同时也省去了特征交叉工作。

当 embedding 矩阵稀疏且高秩时，很难非常效率的学习出低维度的表示（如 user 有特殊的爱好或 item 比较小众），从而可能过度泛化，给出完全不相关的推荐，准确性不能得到保证。

#### 联合训练
将 LR 和 DNN 联合训练，二者的输出通过加权方式合并到一起，并通过 logistic loss function 进行最终输出。在训练的时候，根据最终的 loss 计算出梯度，反向传播到 Wide 和 Deep 两部分中，分别训练自己的参数。

联合训练下 wide 和 deep 的 size 都减小了，wide 只需要填补 deep 的不足就行，只需要较少的交叉特征，不需要全部。

#### 优化器
- wide 部分是用 FTRL（Follow-the-regularized-leader） + L1 正则化学习。
- deep组件是用 AdaGrad 来学习。

## wide&deep 模型应用
### 特征选取
- wide 部分的特征主要是少量的交叉特征，特征可以是任意类型，采用 one-hot 编码为 categorical 类型。
- deep 部分的特征包含全量特征，特征必须转为 dense 类型，包括 numeric, indicator, embeding。

### 网络设计
- 隐藏层设置为[100, 50, 50]。
- 采用正则化和 dropout 降低模型复杂度，防止过拟合，同时减少计算量。

### 离线训练
#### 离线训练流程
- 每天定时执行日志清洗任务，将当天的曝光和互动日志分别作为正负样本，按照 10:1 的比例生成到正负样本混洗表中。
- 每天定时执行样本生成任务，从正负样本混洗表中，选取最近30天的数据导出到 HDFS 指定路径下，作为训练集，从训练集中截取一小部分作为验证集。
- 提交分布式训练任务，训练模型。
- 模型训练完成后，将最近训练的 5 个 checkpoint 中 auc 最高的那个模型推送到线上服务指定路径下。

<img src="images/train.png" alt="" width="542" height="724" align="bottom" />

#### 离线训练示例
[wide_deep](../../../../ml/tree/master/wide_deep)

#### 分布式训练
##### PS 架构
PS（Parameter server）架构是深度学习最常采用的分布式训练架构。在PS架构中，集群中的节点被分为两类：parameter server 和 worker。其中 parameter server存放模型的参数，而 worker负责计算参数的梯度。在每个迭代过程，worker 从 parameter sever 中获得参数，然后将计算的梯度返回给 parameter server，parameter server 聚合从 worker 传回的梯度，然后更新参数，并将新的参数广播给 worker。

##### 分布式 tensorflow
推荐使用 TensorFlow Estimator API 来编写分布式训练代码，理由如下：
- 开发方便，比起 low level 的 api 开发起来更加容易。
- 可以方便地和其他的高阶 API 结合使用，比如 Dataset、FeatureColumns、Head 等。
- 模型函数 model_fn 的开发可以使用任意的 low level 函数，依然很灵活。
- 单机和分布式代码一致，且不需要考虑底层的硬件设施。
- 可以比较方便地和一些分布式调度框架（比如 xlearning）结合使用。

分布式训练时，需要指定 worker_num 和当前实例分配的 worker_index，用于创建对应的train和eval对象，最后调用 tf.estimator.train_and_evaluate 进行分布式训练。
```
    train_spec = tf.estimator.TrainSpec(input_fn=lambda:input_data.train_data_input(TRAIN_FILE, FLAGS.batch_size, num_workers, worker_index),max_steps=train_max_steps)
    eval_spec = tf.estimator.EvalSpec(input_fn=lambda:input_data.eval_data_input(EVAL_FILE, FLAGS.batch_size), start_delay_secs=100, throttle_secs=150, exporters=exporter)
    result = tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)
```

### 线上预估
#### 架构设计
- 使用 brpc 框架取代原生的 tensorflow-serving。
- 收到投放引擎的 predict 请求后，首先根据用户 uid 和广告ID 去 laser 特征数据库中查询模型调用的各种特征。
- 根据模型格式要求，将特征拼接成调用模型的参数，支持通过 batch_size 并行处理多个广告，这里最大为 600.
- 基于模型文件创建 session，并调用 session.run(inputs, "head/predictions/probabilities:0", &output) 发起 predict 调用。

<img src="images/predict.png" alt="" width="400" height="406" align="bottom" />

#### 模型更新策略
- 有新模型推送时，调用 reload 通知接口。
- server 启用 RCU（新旧副本切换）机制执行线上模型更新。

### 粗排优化策略
#### 进入模型规则
基于性能考虑，每次参与 predict 的 batch size 最大 600 个，基于以下规则选取进入模型的广告：
- 根据预算从高到低排序，选取预算排名前 300 的进入模型。
- 剩余 300 个名额使用随机选取策略。

#### 预算平滑
参考 [ad_pacing](ad_pacing.md)

#### 排序规则
根据模型返回的 ctr 从高到低排序，取 top100 返回。

### 性能优化
本项目涉及到从laser获取特征，是一个io密集型和计算密集型服务。
- IO问题

### 系统的问题
#### 人群基础画像准确率低
DMP人群基础画像挖掘的不准，导致模型运行在不真实的数据之上，影响模型效果。

#### 训练时无法还原实时特征
模型训练时，一些历史互动率之类的实时特征，无法还原到样本发生时刻的数值，导致 label 和实时特征不匹配。

#### 实时特征实时性不强
线上模型预测时，使用的实时特征（用户/广告互动率）受制于特征工程统计延迟，会有1个小时以上的延迟，造成结果不准。

#### 性能一般
线上模型预测的性能一般，导致 batch 限制在600以下，其余候选无法入模型，全局上达不到最优解。
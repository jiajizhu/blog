# consul 详解
## 概述
consul 是 HashiCorp 公司推出的开源工具，是一个类似于 zookeeper 的分布式协调服务。

## consul 功能特点
- 服务发现：提供 http 和 dns 查询接口，查询指定服务的可用主机列表。
- 健康检查：服务注册成功后，本地 consul-client 会定时检查服务运行状态，并更新到 consul-server。
- k/v 存储：支持通过 http 接口调用创建和删除 k/v 节点，可用于动态配置、功能标记等。
- 多数据中心：支持多个广域网之间进行服务发现，zookeeper 不支持多数据中心。
- web-ui：提供 web 界面可以查看服务的运行情况。

## consul 集群介绍
consul 支持多数据中心，单个数据中心中，consul 分为 client 和 server 两种 agent 节点。

各 agent 通过 gossip 协议（流言协议）维护成员关系，也就是某个 agent 要知道集群中其他 agent 以及属于 client 还是 server。

所有 agent 节点都可以接受注册和查询，并对注册的服务进行健康检查。

### 各角色功能
- datacenter：根据局域网划分，内部会采用广播进行通信，注意这里不是按机房划分，因为多机房也可以是局域网。
- server-leader：一个集群只能有一个 leader，处理部分读请求和全部的写请求，并将写操作同步给其他 server-follower。
- server-follower：一个集群可以有多个 follower，只处理读请求，并接收 leader 的写同步，在 leader 故障时参与选举。
- client：无状态的，将本地的注册和查询请求通过 RPC 转发给server，本身不做任何持久化，也不参与选举。

## 默认端
- tcp/8300：用于服务器节点，agent通过该端口调用server端节点。
- tcp/udp/8301：用于单个数据中心所有节点之间的互相通信，它使整个数据中心能够自动发现服务器地址，分布式检测节点故障，事件广播
- tcp/udp/8302：跨数据中心请求。
- 8500：http协议，用于api接口或者web ui访问。
- 8600：dns端口，可以通过节点名查询节点信息。

## 运行 consul
### 运行 client 示例
```
/usr/local/bin/consul agent -config-dir=/etc/consul.d/

/etc/consul.d/consul.json 配置文件格式：
{
  "datacenter": "vad",
  "data_dir": "/data0/vad/consul",
  "node_name": "10.75.6.120",
  "bind_addr": "10.75.6.120",
  "retry_join": ["10.41.22.154","10.41.22.155","172.16.108.56","172.16.108.57","172.16.106.164"],
  "disable_update_check": true,
  "client_addr": "0.0.0.0",
  "enable_script_checks": true
}
```

### 注册服务示例
```
curl -XPUT -H "X-Consul-Token: xxx" --data @account-recommend.json http://localhost:8500/v1/agent/service/register
```

### 注销服务示例
```
curl -XPUT -H "X-Consul-Token: xxx" http://localhost:8500/v1/agent/service/deregister/account-recommend
```

### 服务发现
#### dns 接口
通过 dig 命令可以直接获取服务的主机列表。可以利用 dns 缓存（dnsmasq）减轻 consul-server 的访问压力，但是也会导致访问到不可用的服务。使用时需要根据实际访问量和容错能力确定 DNS 缓存方案。
```
dig @127.0.0.1 -p 8600 account-recommend.service.consul
```

#### watches 接口
watches 是查看指定数据信息的一种方法，比如查看 nodes列表、键值对、健康检查。watches 在调用 http api 接口使用阻塞队列，支持请求参数 index 标识当前 watch 的版本号，如果有变化立即响应。

##### 请求
```
http://127.0.0.1:8500/v1/health/service/account-recommend?passing=true&wait=5s&index=9831624186
```

##### 返回
```
[{
	"Node": {
		"ID": "0bc632ca-0631-2db5-3db0-b90b997f0ed7",
		"Node": "10.75.6.120",
		"Address": "10.75.6.120",
		"Datacenter": "vad",
		"TaggedAddresses": {
			"lan": "10.75.6.120",
			"wan": "10.75.6.120"
		},
		"Meta": {
			"consul-network-segment": ""
		},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Service": {
		"ID": "account-recommend",
		"Service": "account-recommend",
		"Tags": [],
		"Address": "",
		"Meta": {},
		"Port": 12099,
		"Weights": {
			"Passing": 1,
			"Warning": 1
		},
		"EnableTagOverride": false,
		"ProxyDestination": "",
		"Proxy": {},
		"Connect": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Checks": [{
		"Node": "10.75.6.120",
		"CheckID": "serfHealth",
		"Name": "Serf Health Status",
		"Status": "passing",
		"Notes": "",
		"Output": "Agent alive and reachable",
		"ServiceID": "",
		"ServiceName": "",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}, {
		"Node": "10.75.6.120",
		"CheckID": "service:account-recommend",
		"Name": "Service 'account-recommend' check",
		"Status": "passing",
		"Notes": "",
		"Output": "TCP connect 127.0.0.1:12099: Success",
		"ServiceID": "account-recommend",
		"ServiceName": "account-recommend",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}]
}, {
	"Node": {
		"ID": "a0cff1ef-f6c9-0960-04f5-60e984ef5136",
		"Node": "10.77.96.167",
		"Address": "10.77.96.167",
		"Datacenter": "vad",
		"TaggedAddresses": {
			"lan": "10.77.96.167",
			"wan": "10.77.96.167"
		},
		"Meta": {
			"consul-network-segment": ""
		},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Service": {
		"ID": "account-recommend",
		"Service": "account-recommend",
		"Tags": [],
		"Address": "10.77.96.167",
		"Meta": null,
		"Port": 12099,
		"Weights": {
			"Passing": 1,
			"Warning": 1
		},
		"EnableTagOverride": false,
		"ProxyDestination": "",
		"Proxy": {},
		"Connect": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Checks": [{
		"Node": "10.77.96.167",
		"CheckID": "serfHealth",
		"Name": "Serf Health Status",
		"Status": "passing",
		"Notes": "",
		"Output": "Agent alive and reachable",
		"ServiceID": "",
		"ServiceName": "",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}, {
		"Node": "10.77.96.167",
		"CheckID": "service:account-recommend",
		"Name": "Service 'account-recommend' check",
		"Status": "passing",
		"Notes": "",
		"Output": "TCP connect localhost:12099: Success",
		"ServiceID": "account-recommend",
		"ServiceName": "account-recommend",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}]
}, {
	"Node": {
		"ID": "be47c852-bf61-3624-fdac-d6b5eaf3ffe0",
		"Node": "10.77.96.86",
		"Address": "10.77.96.86",
		"Datacenter": "vad",
		"TaggedAddresses": {
			"lan": "10.77.96.86",
			"wan": "10.77.96.86"
		},
		"Meta": {
			"consul-network-segment": ""
		},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Service": {
		"ID": "account-recommend",
		"Service": "account-recommend",
		"Tags": [],
		"Address": "",
		"Meta": {},
		"Port": 12099,
		"Weights": {
			"Passing": 1,
			"Warning": 1
		},
		"EnableTagOverride": false,
		"ProxyDestination": "",
		"Proxy": {},
		"Connect": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	},
	"Checks": [{
		"Node": "10.77.96.86",
		"CheckID": "serfHealth",
		"Name": "Serf Health Status",
		"Status": "passing",
		"Notes": "",
		"Output": "Agent alive and reachable",
		"ServiceID": "",
		"ServiceName": "",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}, {
		"Node": "10.77.96.86",
		"CheckID": "service:account-recommend",
		"Name": "Service 'account-recommend' check",
		"Status": "passing",
		"Notes": "",
		"Output": "TCP connect 127.0.0.1:12099: Success",
		"ServiceID": "account-recommend",
		"ServiceName": "account-recommend",
		"ServiceTags": [],
		"Definition": {},
		"CreateIndex": 9831624186,
		"ModifyIndex": 9831624186
	}]
}]
```

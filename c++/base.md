# c++ 基础知识汇总
### c++对象内存模型
#### 普通对象模型
- 非静态成员变量：定义在对象体内，每个实体一份，为当前对象专有。
- 静态成员变量：定义在程序的静态存储区，只有一份实体，该类所有对象共享。
- 成员函数：定义在程序的代码段中，只有一份实体，该类所有对象共享。
- 类中嵌套的结构体类型：与放在类外面定义的类型除了定义域之外没有本质区别，遵循类内部的访问规则。

#### 继承和虚函数对象模型
- 继承基类的成员变量：作为对象自己专用的数据。
- 继承基类的非静态成员函数：作为类的成员函数一样访问。
- 虚函数：所有虚函数的地址都存放在 vtable 里，所有包含虚函数的类对象，都会有一个 vtable 指针。 

#### 虚函数指针的排列顺序
- 如果虚函数是第一次出现，则把它的函数地址指针依次插入到vtable的尾部。
- 派生类重写了虚函数，它的位置和基类中一致，不会因为继承层次的改变而改变。
- 派生类没有改写的基类虚函数被继承，位置与原先在vtable中的位置相同。

### static关键字的作用
#### 全局变量/函数
被修饰全局变量/函数作用域仅为当前文件，其他文件不可见。

#### 局部变量
局部变量定义由堆栈改为静态存储区，函数调用返回后不会被回收。

#### 类成员变量
修饰成员变量使所有的对象只保存一个该变量，而且不需要生成对象就可以访问该成员，在类对象初始化之前创建。

#### 类成员函数
修饰成员函数使得不需要生成对象就可以访问该函数，但是在 static 函数内不能访问非静态成员。

### c++中的四种 cast 转换
#### const_cast
const修饰的指针或引用，因为其const属性，无法修改指向的值，const_cast用于去除其const属性。只对指针或引用有用，普通常量不可修改。

#### static_cast
相当于C语言里面的强制类型转换，比如非const转const(反过来不行)，void*和指针互转，派生类转基类，基类转派生类(不安全)。

#### dynamic_cast 
用于派生类和基类指针或引用的互转，在运行时会检查是否能转成功，不能时返回空指针或者抛出异常(针对引用抛出异常)，性能较低，不推荐使用。

#### reinterpret_cast
几乎什么都可以转，比如将int转指针，可能会出问题，尽量少用。

### 指针和引用
- 指针由自己的一块变量空间，引用只是一个别名，编译器不会为引用分配变量空间，而是会将引用替换为被引起对象的地址。
- 指针可以被改变指向其他对象，引起必须被初始化且不能再改变引用对象。

### 内联函数
- 相当于宏，却比宏多了类型检查，真正具有函数特性，使用时减少了函数调用的开销，效率高。
- 内联会造成代码膨胀，消耗更多的内存空间；是否内联，程序员不可控，由编译器规则决定。
- inline 关键字要加在函数体定义处才有效，只在函数声明时用无效。
- 编译器一般不内联包含循环、递归、switch 等复杂操作的内联函数

### volatile
volatile 告诉编译器不要对该对象进行优化，每次访问时都必须从内存中取出值（编译器优化下，变量可能保存在寄存器中）。

volatile 保证是变量在多线程中的可见性，但是不能保证原子性，不能解决多线程i++问题，只是保证每次读到最新版本。

### 内存对齐
编译器会对 struct 内部变量按照定义的顺序进行内存对齐，对齐的规则是每个变量的偏移地址都是对齐值的整数倍。

对齐值取自身类型 size 和 #pragma pack 指定值较小的那个，inux下默认#pragma pack(4)。

```
#pragma pack(4)
struct Test
{
	char a;    // 按 1 对齐，偏移为0，占用4字节
	int b;     // 按 4 对齐，偏移为4，占用4字节
	short c;   // 按 2 对齐，偏移为8，占用4字节
};
共占用12个字节

struct Test
{
	int b;    // 按 1 对齐，偏移为0，占用4字节
	short c;  // 按 2 对齐，偏移为4，占用2字节
	char a;   // 按 1 对齐，偏移为8，占用1字节
};
占用8个字节
```

#### 为什么要内存对齐
- 性能原因：处理器一般按照双字节或者四字节的存取粒度访问物理内存，内存对齐的变量（int,long），一次就可以读到。

### struct 和 class的区别
- 默认是成员变量访问权限不同，struct 是 public，而 class 是 private。
- 默认的继承访问权限，struct 是 public 的，class 是 private 的。
- 其他都一样，struct也可以实现继承和多态。

### 为什么C++基类的析构函数必须是虚函数？为什么C++默认的析构函数不是虚函数
当基类指针指向子类对象，在释放基类指针时，如果基类析构函数为虚函数，则会先先调用子类的析构函数，再调用基类的析构函数，否则只调用基类析构。

虚函数需要额外的虚函数表和虚表指针，占用额外内存，所以C++默认的析构函数不是虚函数。

### 虚函数的实现
在有虚函数的类中，类的最开始部分是一个虚函数表的指针，这个指针指向一个虚函数表，表中放了虚函数的地址，实际的虚函数在代码段(.text)中。

当子类继承了父类的时候也会继承其虚函数表，当子类重写父类中虚函数时候，会将其继承到的虚函数表中的地址替换为重新写的函数地址。

使用虚函数，运行时要查虚函数表获取函数地址，会增加访问内存开销，降低效率。

### C++函数栈空间的最大值
默认是 8M，可以通过 ulimit -s xxx 调整大小

#### RTTI 机制
RTTI 是指运行时类型信息，它提供了运行时确定对象类型的方法，主要涉及两个操作符：typeid 和 dynamic_cast。
typeid 不但可以在运行时获取基本类型，还可以通过基类指针或引用，获取所指对象的实际类型。
```
#include <iostream>
#include <typeinfo>

int val = 100;
std::cout << typeid(val).name() << std::endl; // 输出i表示整数
```

### 在C++程序中调用被C 语言修饰的函数，为什么要加extern “C”？
因为 C++ 支持重载，编译生成的函数符号和 C 语言编译出来不一样，带有类型信息，如果 C++ 直接去链接 C 语言编译的库，会找不到函数符号，加上 extern “C” 之后，C++ 会按 C 的方式去查找函数符号。

### 关于默认参数
- 函数声明时加入默认值，调用时可忽略该参数直接使用默认值，提高编码效率。。
- 某个默认参数的后面所有参数均需要默认值，因此经常把带有默认值的参数放在其他参数后面。

### 怎么让一个对象只能在堆上分配，不能在栈上分配
将析构函数设置为私有，编译器无法调用析构函数，使用临时变量的方式去分配时就会编译报错

### 怎么让一个对象只能在栈上分配，不能在堆上分配
重载new运算符，并设置为private私有，这样用new去创建对象时，就会编译报错。

### 拷贝构造函数的参数为什么必须是引用类型，不能是值传递
如果是值传递，在参数传递拷贝过程中，会递归调用拷贝构造函数，陷入死循环，采用引用传递可以避免。

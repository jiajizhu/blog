# 排序
## 概述
排序算法的常规思路是遍历每个元素，并与其他元素比较大小，最后放入有序区间中，时间复杂度是O(n^2)，包括冒泡排序，选择排序，插入排序。

对常规思路就行优化，仍然是遍历所有元素，但是尽量的减少比较的次数，时间复杂度是O(nlogn)，包括归并、堆排序、快速排序。

稳定排序是指相等的元素再排序后相对位置不变，稳定排序有冒泡排序，插入排序，归并排序。

## 常见排序算法
### 算法特点
- 冒泡排序：稳定排序，时间复杂度O(n^2)
- 选择排序：不稳定排序，时间复杂度O(n^2)，选择排序的交换次数会比冒泡排序少，所以性能比冒泡排序高。
- 插入排序：稳定排序，最坏情况数组逆序，时间复杂度O(n^2)，最好情况数组顺序则是O(n)
- 归并排序：稳定排序，时间复杂度为O(nlogn)，归并排序需要O(n)的额外空间，采用分治法。
- 堆排序：不稳定排序，时间复杂度为O(nlogn)，堆排序其实是变相的选择排序，构建一个大(小)顶堆，每次从堆顶选择一个最大(小)数，放到数组末尾。
- 快速排序：不稳定排序，时间复杂度平均为O(nlogn)，当待排列有序(正序或逆序)时，时间复杂度为O(n^2)，当序列随机时，排序效果最好。快速排序虽然时间复杂度不稳定，但是和堆排序比，无效交换次数少，性能最高，而归并排序不是原地排序，需要额外的空间。

### 算法实现
#### 冒泡排序
```
void bubble_sort(int array[], int len) {
	for (int i = 0; i < len - 1; i++) {
		for (int j = 0; j < len - 1 - i; j++) {
			if (array[j] > array[j+1]) {
				int tmp = array[j];
				array[j] = array[j+1];
				array[j+1] = tmp;
			}
		}
	}
}
```

#### 选择排序
```
void select_sort(int arr[], int len) {
	for (int i = 0; i < len; i++) {
		int min_pos = i;
		for (int j = i+1; j < len; j++) {
			if (arr[j] < arr[min_pos]) {
				min_pos = j;
			}		
		}
		int tmp = arr[i];
		arr[i] = arr[min_pos];
		arr[min_pos] = tmp;
	}	
}
```

#### 插入排序
```
void insert_sort(int arr[], int len) {
	for (int i = 0; i < len; i++) {
		for (int j = i; j > 0; j--) {
			if (arr[j] < arr[j-1]) {
				int tmp = arr[j];
				arr[j] = arr[j-1];
				arr[j-1] = tmp;
			} else {
				break;
			}
		}
	}	
}
```

#### 归并排序
```
void merge(int arr[], int tmp[], int l, int r, int mid) {
	int i = l, j = mid+1, k = l;	
	while (i <= mid && j <= r) {
		if (arr[i] < arr[j]) {
			tmp[k++] = arr[i++];
		} else {
			tmp[k++] = arr[j++];
		}
	}

	while (i <= mid) {
		tmp[k++] = arr[i++];
	}

	while (j <= r) {
		tmp[k++] = arr[j++];
	}
	
	for (i = l; i < k; i++) {
		arr[i] = tmp[i];
	}
}

// 归并排序：稳定排序，时间复杂度固定位O(nlogn), 但是需要O(n)的额外空间
void merge_sort(int arr[], int tmp[], int l, int r) {
	if (l >= r) {
		return;
	}

	int mid = l + (r-l)/2;
	merge_sort(arr, tmp, l, mid);
	merge_sort(arr, tmp, mid+1, r);
	merge(arr, tmp, l, r, mid);
}
```

#### 堆排序
```
// 从i结点往下调整堆，保证当前序列(i~len)和子树满足大顶堆
void heap_adjust(int arr[], int i, int len) {
	// 比较左右子树，当大于当前结点时，交换
	while (2*i+1 < len) {
		int j = 2*i+1;
		if (j+1 < len && arr[j+1] > arr[j])	// 左右子树比较
			j++;
		
		// 子树和当前结点比较，如果发生交换，则要考虑交换后的子节点是否满足大顶堆，需要循环继续调整
		if (arr[j] > arr[i]) {
			int tmp = arr[j];
			arr[j] = arr[i];
			arr[i] = tmp;
			i = j;
		} else {
			break;
		}
	}
}


// 完全二叉树：当结点个数为n，树深度为h时，第h-1层结点为满的(满足n=2^h-1)，第h层结点满的或者全部靠左
// 堆定义：一个完全二叉树中，任意父结点总是大于等于(小于或等于)任何一个子节点，则为大顶堆(小顶堆),注意因为左右子节点大小关系不确定，所以堆不是有序的
// 堆采用数组存储，第i个结点的左右子结点分别是2i+1和2i+2
// 堆排序，不稳定排序，时间复杂度固定位O(nlogn)
// 堆排序类似选择排序，首先构建一个大顶堆，然后每次取堆顶元素(最大值)，和末尾元素交换，得到一个从后往前的有序序列
void heap_sort(int arr[], int len) {
	// 1.构建大顶堆,叶子节点不用管，从第一个非叶子节点开始调整，全部调整完后，就是一个大顶堆(无序的)
	// 第一个非叶子节点的下标为: n/2-1
	for (int i = len/2-1; i >=0; i--) {
		heap_adjust(arr, i, len);
	}

	// 2.类似选择排序，循环把堆顶元素(最大值，也是第一个元素)和当前最后一个元素交换，则得到一个从后往前的有序序列,交换完成后，继续调整剩余元素，保证仍然满足最大堆
	for (int i = len - 1; i > 0; i--) {
		// 交换堆顶和当前元素
		int tmp = arr[0];
		arr[0] = arr[i];
		arr[i] = tmp;
		heap_adjust(arr, 0, i);
	}
}
```

#### 快速排序
```
void quick_sort(int array[], int l, int r) {
	if (l >= r) {
		return;
	}

	int base = array[l];
	int i = l;
	int j = r;

	while (i < j) {
		while (i < j && array[j] >= base)
			j--;
		array[i] = array[j];

		while (i < j && array[i] < base)
			i++;
		array[j] = array[i];
	}

	array[i] = base;
	quick_sort(array, l, i-1);
	quick_sort(array, i+1, r);
}
```

## 常见排序题
### 三数字排序
一个数组只含 0, 1, 2 三种数，对这个数组排序，要求只能扫描一遍数组。

#### 思路
- 采用三指针解决，定义左右两个指针分别从两端寻找非0和非2的值，然后第三个指针遍历元素，进行位置交换。

#### 示例
```
输入：[0,2,1,1,0,2,0,0,1,2,1,2,2]
输出：[0,0,0,0,1,1,1,1,2,2,2,2,2]
```

#### 代码
```
void sort(std::vector<int>& nums) {
        int l = 0, r = nums.size()-1;
        int k = 0;

        // 第三个指针k从左到右遍历
        while (k < r) {
                if (nums[k] == 2) {
                        // 从右边寻找一个非2的值
                        while (k < r && nums[r] == 2)
                                r--;
                        swap(nums[k], nums[r]);
                } else if (nums[k] == 0) {
                        // 从左边寻找一个非0的值
                        while (l < k && nums[l] == 0)
                                l++;
                        swap(nums[k], nums[l]);
                }

                // 交换后的k值有可能是0,1,2，其中0或者2都需要再处理，所以k值不能++，如果是1则k移动到下一个元素
                // 如果k和l相等，说明k以及左边元素已经全部是0了，当前k的0值不需要再处理，k移动到下一个元素
                if (nums[k] == 1 || k == l)
                        k++;
        }

}
```
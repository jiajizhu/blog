# 动态规划详解
## 概述
动态规划的应用场景是，目标问题的最优解，依赖于子问题的最优解，通过穷举子问题递推出当前问题的解。

动态规划求解的关键环节是确定dp数组的含义，然后推导出递推公式。

## 奶牛产子问题
假设母牛每年生一只母牛，新出生的母牛成长三年也能每年生一只母牛，求N年后，母牛的数量，假设第1头牛当年开始产子

### 思路
今年的母牛数 = 去年的母牛数 + 三年前的母牛数(产子数)，这道题的核心就是要明白新增牛崽的个数等于三年前母牛的个数。

### 拓展
如果每头奶牛出生后第10年死去，则f(n) = f(n-1) + f(n-3) - f(n-10)

### 代码
```
int GetCowNum(int year) {
    vector<int> dp;
    dp.resize(n+1);
    dp[0] = 0;
    for (int i = 1; i <= n; i++) {
        if (i < 4)
            dp[i] = 2;
        else
            dp[i] = dp[i-1] + dp[i-3];
    }
    return dp[n];
}
```

## 凑零钱问题
给你k种面值的硬币，面值分别为 c1, c2 ... ck，每种硬币的数量无限，再给一个总金额 amount，问你最少需要几枚硬币凑出这个金额，如果不可能凑出，算法返回 -1 

### 思路
比如金额为11，遍历到的当前面值为5，则凑齐金额11的硬币数就等于金额6的最小硬币数再加上1，但是这个解不一定是最小值，所以在遍历面值的过程中要判断取最小值。

### 拓展
这里初始化dp的值为amount+1，因为任何值的解都不可能大于amount，所以可以用amount+1来代表初始值。为什么不用-1？因为循环判断里面会用到min函数，-1有歧义会导致错误。

```
int getCoinSize(vector<int> &coins, int amount) {
    vector<int> dp;
    dp.resize(amount+1, amount+1);
    amount[0] = 0;
    for (int i = 1; i <= amount; i++) {
        for (int j = 0; j < coins.size(); j++) {
            if (coins[j] > i) {
               continue;
            }
            dp[i] = min(dp[i], 1 + dp[i - j]);
        }
    }
    return (dp[amount] == amount + 1) ? -1 : dp[amount];
} 
```

## 凑零钱问题2
给你k种面值的硬币，面值分别为 c1, c2 ... ck，每种硬币的数量无限，再给一个总金额 amount，问你最多有几种方法凑出这个金额。

### 思路
类似于爬台阶，第n个金额的方法数分别等于n-c1,n-c2...n-ck对应的方法数之和，当n=ck的时候，这个时候n-ck为0，所以初始化dp[0]为1。

### 代码
```
int getCoinNum(vector<int> &coins, int amount) {
    if (amount == 0)
        return 0;
        
    vector<int> dp;
    dp.resize(amount+1);
    dp[0] = 1;
    for (int i = 1; i <= amount; i++) {
        for (int j = 0; j < coins.size(); j++) {
            if (coins[j] > i)
                continue;
            dp[i] += dp[i-coins[j]];    
        }
    }
    return dp[amount];
}
```

## 213. 打家劫舍 II
有首位相连的n个房间，每个房间存放不定金额，相邻两个房间有连通警报，偷两个相邻房间时会报警，怎样偷到最多的钱。

### 思路
- 采用动态规划，dp[i]代表对第i个房间下手的最高金额，递推公式 dp[i] = nums[i] + max(dp[i-2], dp[i-3])
- 本题的难点是首尾房间是连着的，所以需要分别求出不带首房间的最大金额，和不带尾房间的最大金额，然后比较二者大小

### 代码
```
class Solution {
public:
    int rob(vector<int>& nums) {
        if (nums.size() == 0)
            return 0;
        if (nums.size() == 1)
            return nums[0];

        return max(dorob(nums, 0, nums.size()-2), dorob(nums, 1, nums.size()-1));
    }

private:
    int dorob(vector<int>& nums, int s, int e) {
        int len = e - s + 1;
        if (len == 1)
            return nums[s];
        if (len == 2)
            return max(nums[s], nums[s+1]);
        if (len == 3)
            return max(nums[s+2] + nums[s], nums[s+1]);

        vector<int> dp(len);
        dp[0] = nums[s];
        dp[1] = nums[s+1];
        dp[2] = nums[s] + nums[s+2];
        for (int i = s + 3; i <= e; i++) {
            int pos = i - s;
            dp[pos] = max(dp[pos-2], dp[pos-3]);
            dp[pos] += nums[i];
        }
        return max(dp[len-1], dp[len-2]);
    }
};
```

## 714. 买卖股票的最佳时机含手续费
给定一个整数数组 prices，其中第 i 个元素代表了第 i 天的股票价格，可以无限次交易，返回获得利润的最大值。

非负整数 fee 代表了交易股票的手续费用。

### 示例
```
输入: prices = [1, 3, 2, 8, 4, 9], fee = 2
输出: 8
解释: 能够达到的最大利润:  
在此处买入 prices[0] = 1
在此处卖出 prices[3] = 8
在此处买入 prices[4] = 4
在此处卖出 prices[5] = 9
总利润: ((8 - 1) - 2) + ((9 - 4) - 2) = 8.
```
### 思路
- 动态规划，二维数组dp[i][0]表示第i天卖出的最大收益，dp[i][1]表示第i天买入的最大收益
- 递推公式：dp[i][0] = max(dp[i-1][1] + prices[i], dp[i-1][0])；dp[i][1] = max(dp[i-1])
- 费率在买入时扣除

### 代码
```
int maxProfit(vector<int>& prices, int fee) {
    int len = prices.size();
    vector<vector<int>> dp(len, vector<int>(2, 0));
    dp[0][0] = 0;	// 第一天不能卖出，收益为0
    dp[0][1] = 0 - prices[0] - fee; // 买入后收益，扣除费率
    
    for (int i = 1; i < len; i++) {
        dp[i][0] = max(dp[i-1][0], dp[i-1][1] + prices[i]);
        dp[i][1] = max(dp[i-1][0] - prices[i] - fee, dp[i-1][1]);
    }
    
    return dp[len-1][0];
}
```

## 1143. 最长公共字串（LCS）
给定两个字符串 text1 和 text2，返回这两个字符串的最长公共子序列的长度。

### 示例
```
输入：text1 = "abcde", text2 = "ace" 
输出：3  
解释：最长公共子序列是 "ace"，它的长度为 3。

输入：text1 = "abc", text2 = "abc"
输出：3
解释：最长公共子序列是 "abc"，它的长度为 3。
```

### 思路
- 采用动态规划，dp[i][j]的含义是，text1前i子串和text2前j子串的最长公共序列长度。
- 递推公式1：如果text1[i] == text[j]，dp[i][j] = dp[i-1][j-1]。
- 递推公式2：如果text1[i] != text[j]，dp[i][j] = max(dp[i][j-1], dp[i-1][j])
- dp初始化时，多初始化一行一列，从dp[1][1]开始填

### 代码
```
int longestCommonSubsequence(string text1, string text2) {
    int len1 = text1.size(), len2 = text2.size();
    vector<vector<int>> dp(len1+1, vector<int>(len2+1, 0));

    for (int i = 0; i < len1; i++) {
        for (int j = 0; j < len2; j++) {
            if (text1[i] == text2[j]) {
                dp[i+1][j+1] = dp[i][j] + 1;
            } else {
                dp[i+1][j+1] = max(dp[i][j+1], dp[i+1][j]);
            }
        }
    }
    return dp[len1][len2];
}
```

## 5. 最长回文子串
给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。

### 示例
```
输入: "babad"
输出: "bab"
注意: "aba" 也是一个有效答案。

输入: "cbbd"
输出: "bb"
```

### 思路
- 利用动态规划，dp是个二维数组，dp[l][r]代表l到r是否是回文
- dp[l][r]的递推公式：如果dp[l+1][r-1]为true，且s[l]==s[r]，则dp[l][r]为true，否则为false。
- 双指针遍历，右指针代表结束位置，左指针代表开始位置，根据递推公式设置所有dp的值。
- 如果左右指针相等，说明是一个字符，则为true，如果左右指针相邻，则只需比较左右指针字符是否相等。

### 代码
```
string longestPalindrome(string s) {
    if (s.size() < 2)
        return s;
    
    // 定义最长回文子串的起始位置和长度
    int start  = 0, max_len = 0;

    // 初始化dp二维数组
    vector<vector<bool>> dp(s.size(), vector<bool>(s.size(), false));
    for (int r = 0; r < s.size(); r++) {
        for (int l = 0; l <= r; l++) {
            if (l == r) {
                // 单个字符为回文
                dp[l][r] = true;
            } else if (s[r] == s[l]) {
                // 左右字符相同，判断是否为相邻字符或者中间字符为回文
                if (l+1 == r || dp[l+1][r-1])
                    dp[l][r] = true;
            }

            // 更新最长子串
            if (dp[l][r] && r-l+1 > max_len) {
                start = l;
                max_len = r-l+1;
            }
        }
    }
    
    return s.substr(start, max_len);
}
```

## 64. 最小路径和
给定一个包含非负整数的 m x n 网格，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。

说明：每次只能向下或者向右移动一步。

### 示例
```
输入:
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
输出: 7
解释: 因为路径 1→3→1→1→1 的总和最小。
```

### 思路
- 采用动态规划，递推公式：dp[i][j] = min(dp[i-1][j], dp[i][j-1]) + grid[i][j]
- 从左上角到右下角根据递推公式设置dp，考虑到 i 和 j 为 0 的情况。

### 代码
```
int minPathSum(vector<vector<int>>& grid) {
    if (grid.size() == 0 || grid[0].size() == 0)
        return 0;

    int rows = grid.size();
    int cols = grid[0].size();

    // 初始化dp
    vector<vector<int>> dp(rows, vector<int>(cols, 0));
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            if (i == 0 && j == 0) {
                dp[i][j] = grid[i][j];
            } else if (i == 0) {
                dp[i][j] = dp[i][j-1] + grid[i][j];
            } else if (j == 0) {
                dp[i][j] = dp[i-1][j] + grid[i][j];
            } else {
                dp[i][j] = min(dp[i][j-1], dp[i-1][j]) + grid[i][j];
            }
        }
    }

    return dp[rows-1][cols-1];
}
```

## 152. 乘积最大子数组
给你一个整数数组 nums ，请你找出数组中乘积最大的连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。

### 示例
```
输入: [2,3,-2,4]
输出: 6
解释: 子数组 [2,3] 有最大乘积 6。
```

### 思路
- 采用动态规划法，定义dpMax[i]和dpMin[i]，分别代表以第 i 个数结尾的 乘积最大(小)的连续子序列

### 代码
```
int maxProduct(vector<int>& nums) {
    int size = nums.size();
    vector<int> dpMax(size, 1);
    vector<int> dpMin(size, 1);

    int max_val = nums[0];
    dpMax[0] = nums[0];
    dpMin[0] = nums[0];
    for (int i = 1; i < size; i++) {
        dpMax[i] = max(max(nums[i], dpMax[i-1]*nums[i]), dpMin[i-1]*nums[i]);
        dpMin[i] = min(min(nums[i], dpMin[i-1]*nums[i]), dpMax[i-1]*nums[i]);
        max_val = max(max_val, dpMax[i]);
    }
    return max_val;
}
```
# 双指针详解
## 概述
双指针是面对数组、链表结构的一种处理技巧，这里“指针”是泛指，不但包括普通指针，还包括索引、迭代器等可用于遍历的游标。

双指针可以分为
- 同方向指针
- 反方向指针
- 快慢指针

##  同方向指针
设定两个指针、从头往尾(或从尾到头)遍历，称之为同方向指针，第一个指针用于遍历，第二个指针满足一定条件下移动。

### 283. 移动零
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
#### 示例:
```
输入: [0,1,0,3,12]
输出: [1,3,12,0,0]
```
#### 思路
- 采用双指针，右指针遍历元素，左指针始终指向第一个0值，遍历数组对非0元素进行交换。
- 遍历过程中，遇到0值跳过，遇到非零值就和左指针交换，难点是要维护左指针始终能指向第一个0值。

#### 代码
```
void moveZeroes(vector<int>& nums) {
    int l = 0, r = 0;
    for (; r < nums.size(); r++) {
        if (nums[r] != 0) {
            if (nums[l] == 0) {
                nums[l] = nums[r];
                nums[r] = 0;
            }
            l++;
        }
    }
}
```

### 26. 删除排序数组中的重复项
给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。

#### 示例1:
```
给定数组 nums = [1,1,2], 

函数应该返回新的长度 2, 并且原数组 nums 的前两个元素被修改为 1, 2。 

你不需要考虑数组中超出新长度后面的元素。
```

#### 示例 2:
```
给定 nums = [0,0,1,1,1,2,2,3,3,4],

函数应该返回新的长度 5, 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4。

你不需要考虑数组中超出新长度后面的元素。
```

#### 思路
- 采用双指针，右指针遍历元素，左指针始终指向无重复序列的最后一个元素
- 每遍历一个元素，都判断是否大于左指针，如果等于左指针，则重复元素直接跳过
- 如果大于左指针，则和左指针的下一个元素进行交换，并将左指针指向下一个元素。

```
int removeDuplicates(vector<int>& nums) {
    if (nums.size() < 2)
        return nums.size();

    int l = 0, r = 0;
    for (; r < nums.size(); r++) {
        if (nums[r] > nums[l]) {
            nums[++l] = nums[r];
        }
    }

    return l+1;
}
```

### 80. 删除排序数组中的重复项 II
给定一个排序数组，你需要在原地删除重复出现的元素，使得每个元素最多出现两次，返回移除后数组的新长度。
#### 示例
```
给定 nums = [0,0,1,1,1,1,2,3,3],

函数应返回新长度 length = 7, 并且原数组的前五个元素被修改为 0, 0, 1, 1, 2, 3, 3 。

你不需要考虑数组中超出新长度后面的元素。
```
#### 思路
- 采用上一题思路，只不过移动时判断前一个元素是否重复

#### 代码
```
int removeDuplicates(vector<int>& nums) {
    if (nums.size() < 3)
        return nums.size();

    int l = 0, r = 0;
    for (r = 1; r < nums.size(); r++) {
        if (nums[r] != nums[l]) {
            nums[++l] = nums[r];
        } else {
            // 相等时，判断l前一个元素
            if ((l == 0 || nums[l] != nums[l-1])) {
                nums[++l] = nums[r];
            }
        }
    }

    return l+1;
}
```

## 反方向指针
若双指针其中一个从头开始、另一个从尾开始，两者往中间遍历，这种使用方法我称之为反方向指针。

### 344. 反转字符串
编写一个函数，其作用是将输入的字符串反转过来。输入字符串以字符数组 char[] 的形式给出。

#### 示例
```
输入：["h","e","l","l","o"]
输出：["o","l","l","e","h"]

输入：["H","a","n","n","a","h"]
输出：["h","a","n","n","a","H"]
```
#### 思路
采用左右两个双指针，从两头往中间交换赋值。

#### 代码
```
void reverseString(vector<char>& s) {
    int l = 0;
    int r = s.size()-1;
    
    while (l < r) {
        swap(s[l++], s[r--]);
    }
}
```

### 125. 验证回文串
给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。

#### 示例
```
输入: "A man, a plan, a canal: Panama"
输出: true

输入: "race a car"
输出: false
```

#### 思路
双指针判断，注意大小写，先将合法的字符转移到一个vector里面后，再判断

#### 代码
```
 bool isPalindrome(string s) {
    std::vector<char> vec_tmp;
    for (int i = 0; i < s.size(); i++) {
        if (s[i] >= 48 && s[i]<= 57 || s[i] >= 65 && s[i] <= 90)
            vec_tmp.push_back(s[i]);
        if (s[i] >= 97 && s[i] <= 122)
            vec_tmp.push_back(s[i]-32);
    }

    int l = 0, r = vec_tmp.size()-1;
    while (l < r) {
        if (vec_tmp[l++] != vec_tmp[r--])
            return false;
    }
    return true;
}
```

## 应用于有序数列
一些情况下先对数组排序，利用有序这个性质来判别双指针怎么移动。

### 167. 两数之和 II - 输入有序数组
给定一个已按照升序排列 的有序数组，找到两个数使得它们相加之和等于目标数。函数应该返回这两个下标值 index1 和 index2，其中 index1 必须小于 index2。

#### 示例
```
输入: numbers = [2, 7, 11, 15], target = 9
输出: [1,2]
解释: 2 与 7 之和等于目标数 9 。因此 index1 = 1, index2 = 2 。
```

#### 思路
- 左右双指针，判断和是否等于目标数
- 如果大于目标数，则右指针左移，否则左指针右移

#### 代码
```
vector<int> twoSum(vector<int>& numbers, int target) {
    std::vector<int> vec_result;
    int l = 0, r = numbers.size() - 1;
    while (l < r) {
        int sum = numbers[l] + numbers[r];
        if (sum == target) {
            vec_result.push_back(l+1);
            vec_result.push_back(r+1);
            break;
        } else if (sum > target) {
            r--;
        } else {
            l++;
        }
    }
    return vec_result;
}
```

### 15. 三数之和
给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。

#### 示例
```
给定数组 nums = [-1, 0, 1, 2, -1, -4]，

满足要求的三元组集合为：
[
  [-1, 0, 1],
  [-1, -1, 2]
]
```
#### 思路
- 先排序，然后遍历数组元素，每次被遍历的元素作为基准值，然后用双指针寻找合适的另外两个数
- 遍历时要记得跳过重复的数字

#### 代码
```
 vector<vector<int>> threeSum(vector<int>& nums) {
    vector<vector<int>> result;
    if (nums.size() < 3)
        return result;

    // 排序后使用双指针
    sort(nums.begin(), nums.end());
    for (int i = 0; i < nums.size()-2; i++) {
        // 基准数大于0直接结束
        if (nums[i] > 0)
            break;
        
        // 跳过重复数字
        if (i > 0 && nums[i] == nums[i-1])
            continue;

        int l = i + 1;
        int r = nums.size() - 1;

        while (l < r) {
            int sum = nums[i] + nums[l] + nums[r];
            if (sum > 0) 
                r--;
            else if (sum < 0)
                l++;
            else {
                result.push_back({nums[i], nums[l++],nums[r--]});
                
                // 左右值都跳过重复的值
                while (l < r && nums[l] == nums[l-1])
                    l++;
                while (l < r && nums[r] == nums[r+1])
                    r--;
            }
        }
    }
    return result;
}
```

### 16. 最接近的三数之和
给定一个包括 n 个整数的数组 nums 和 一个目标值 target。找出 nums 中的三个整数，使得它们的和与 target 最接近。返回这三个数的和。假定每组输入只存在唯一答案。

#### 示例
```
输入：nums = [-1,2,1,-4], target = 1
输出：2
解释：与 target 最接近的和是 2 (-1 + 2 + 1 = 2) 。
```

#### 思路
- 这道题的最坏情况是暴力穷举，所有的值都组合比较一次，时间复杂度是On^3。
- 将数组排序后，可以减少比较的次数，比如1,2,3,4,5，如果1,2,5组合大于target，则1,[3-4],5这些组合就没必要比较。
- 由于是三个数，可以固定遍历一个数，另外两个数通过双指针两头逼近的方式寻找。

#### 代码
```
int threeSumClosest(vector<int>& nums, int target) {
    // 对数组先排序
    std::sort(nums.begin(), nums.end());

    // 初始化差值为最大
    int min_sum = 0, min_diff = INT_MAX;

    // 遍历固定的一个数，用左右指针寻找另外两个合适的数
    for (int i = 0; i < nums.size()-2; i++) {
        int l = i + 1;
        int r = nums.size() - 1;
        while (l < r) {
            int sum = nums[i] + nums[l] + nums[r];
            int diff = abs(sum - target);
            if (diff < min_diff) {
                min_diff = diff;
                min_sum = sum;
            }

            // 根据值的大小移动指针
            if (sum > target)
                r--;
            else if (sum < target)
                l++;
            else
                return target;
        }
    }
    return min_sum;
}
```
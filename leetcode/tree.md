# 二叉树
## 概述
满足这样性质的树称为二叉树：空树或节点最多有两个子树，称为左子树、右子树， 左右子树节点同样最多有两个子树。

二叉树是递归定义的，因而常用递归/DFS的思想处理二叉树相关问题。

### 二叉树的定义
```
struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};
```

## 递归思想
### 104. 二叉树的最大深度
给定一个二叉树，找出其最大深度。二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。

#### 思路
- 递归求左右子树的各自最大深度，然后取二者最大的，然后加是1（代表本层深度）返回

#### 代码
```
int maxDepth(TreeNode* root) {
    if (!root)
        return 0;
    return max(maxDepth(root->left), maxDepth(root->right)) + 1;
}
```

### 112. 路径总和
给定一个二叉树和一个目标和，判断该树中是否存在根节点到叶子节点的路径，这条路径上所有节点值相加等于目标和。

#### 思路
- 递归左右子树，sum值每次减去当前节点的val
- 注意节点的val有可能是负值，所以一定要递归到叶子节点到底

#### 代码
```
bool hasPathSum(TreeNode* root, int sum) {
    if (!root)
        return false;
    
    if (!root->left && !root->right)
        return sum == root->val ? true : false;
    return hasPathSum(root->left, sum - root->val) || hasPathSum(root->right, sum - root->val);
}
```

### 437. 路径总和 III
给定一个二叉树，它的每个结点都存放着一个整数值。

找出路径和等于给定数值的路径总数。

路径不需要从根节点开始，也不需要在叶子节点结束，但是路径方向必须是向下的（只能从父节点到子节点）。

二叉树不超过1000个节点，且节点数值范围是 [-1000000,1000000] 的整数。

#### 示例：
```
root = [10,5,-3,3,2,null,11,3,-2,null,1], sum = 8

      10
     /  \
    5   -3
   / \    \
  3   2   11
 / \   \
3  -2   1

返回 3。和等于 8 的路径有:

1.  5 -> 3
2.  5 -> 2 -> 1
3.  -3 -> 11
```

#### 思路
- 双重递归，第一重递归所有节点和sum，第二重递归所有节点所在序列
#### 代码
```
void dfs(TreeNode* root, int sum) {
        if (!root)
            return;

        if (root->val == sum ) {
            count_++;
        }

        dfs(root->left, sum - root->val);
        dfs(root->right, sum - root->val);
    }
    int pathSum(TreeNode* root, int sum) {
        if (!root)
            return 0;

        dfs(root, sum);
        pathSum(root->left, sum);
        pathSum(root->right, sum);
        return count_;    
    }
private:
    int count_;
```

### 124. 二叉树中的最大路径和
给定一个非空二叉树，返回其最大路径和。

#### 示例
```
示例 1：

输入：[1,2,3]

       1
      / \
     2   3

输出：6
示例 2：

输入：[-10,9,20,null,null,15,7]

   -10
   / \
  9  20
    /  \
   15   7

输出：42
```

#### 思路
- 考虑到负数，每个节点的最大路径和需要比较：左子树垂直和，右子树垂直和，当前节点值，左子树+右子树+当前节点
- 从根节点开始递归遍历每个节点，计算每个节点的路径和，迭代更新最大路径和。

#### 代码
```
class Solution {
public:
    int maxPathSum(TreeNode* root) {
        max_path_sum = root->val;
        getMaxSum(root);
        return max_path_sum;
    }

private:
    int getMaxSum(TreeNode* root) {
        if (root == NULL)
            return 0;

        int left_sum = getMaxSum(root->left);
        int right_sum = getMaxSum(root->right);

        int max_sum =  max(max(left_sum + root->val, right_sum + root->val), root->val);
        max_path_sum = max(max(max_path_sum, max_sum), left_sum + right_sum + root->val);
        return max_sum;
    }

    int max_path_sum;
};
```

### 543. 二叉树的直径
给定一棵二叉树，你需要计算它的直径长度。一棵二叉树的直径长度是任意两个结点路径长度中的最大值。这条路径可能穿过也可能不穿过根结点。

#### 示例 :
```
给定二叉树

          1
         / \
        2   3
       / \     
      4   5    
返回 3, 它的长度是路径 [4,2,1,3] 或者 [5,2,1,3]
```
#### 思路
- 维护一个计数器，递归计算左右子树的深度，求和后得到一个直径，如果大于计数器，则更新计数器

#### 代码
```
class Solution {
public:
    int diameterOfBinaryTree(TreeNode* root) {
        max_diameter = 0;
        dfs(root);
        return max_diameter;
    }

    int dfs(TreeNode* root) {
        if (!root)
            return 0;
        
        int depth_left = dfs(root->left);
        int depth_right = dfs(root->right);
        int diameter = depth_left + depth_right;
        max_diameter = max(diameter, max_diameter);
        return 1 + max(depth_left, depth_right);

    }
private:
    int max_diameter;
};
```

### 572. 另一个树的子树
给定两个非空二叉树 s 和 t，检验 s 中是否包含和 t 具有相同结构和节点值的子树。

#### 思路
- 双重递归，验证 s 的每个节点，是否和 t 完全一样。

```
class Solution {
public:
    bool isSubtree(TreeNode* s, TreeNode* t) {
        if (!s)
            return false;
        
        return isSametree(s, t) || isSubtree(s->left, t) || isSubtree(s->right, t);
    }

    bool isSametree(TreeNode* s, TreeNode* t) {
        if (s == NULL && t == NULL)
            return true;
        
        if (s == NULL || t == NULL)
            return false;
        
        if (s->val != t->val)
            return false;
        
        return isSametree(s->left, t->left) && isSametree(s->right, t->right);
    }
};
```

### 110. 平衡二叉树判定
给定一个二叉树，判断它是否是高度平衡的二叉树。

一棵高度平衡二叉树定义为：一个二叉树每个节点 的左右两个子树的高度差的绝对值不超过1。

#### 示例
```
给定二叉树 [3,9,20,null,null,15,7]

    3
   / \
  9  20
    /  \
   15   7

```

#### 思路
- 判定左右子树是不是平衡二叉树，判断的同时获取深度，如果左右子树是平滑二叉树，则判断深度差是否大于1

#### 代码
```
class Solution {
public:
    bool isBalanced(TreeNode* root) {
        int deepth = 0;
        return dfs(root, deepth);
    }

private:
    bool dfs(TreeNode* root, int& deepth) {
        if (root == NULL)
            return true;
        
        int deepth_left = 0, deepth_right = 0;
        if (!dfs(root->left,deepth_left))
            return false;
        if (!dfs(root->right, deepth_right))
            return false;
        
        int diff = deepth_left - deepth_right;
        if (diff > 1 || diff < -1)
            return false;
        deepth = max(deepth_left, deepth_right) + 1;
        return true;
    }
```

### 236. 二叉树的最近公共祖先
给定一个二叉树, 找到该树中两个指定节点的最近公共祖先。一个节点也可以是它自己的祖先。

#### 思路
- 递归查找在左右子树上的最近公共祖先，如果左右子树的结果都为空，则当前节点就是最近公共祖先
- 如果左右子树其中一个不为空，则不为空的那个结果就是最近公共祖先。

#### 代码
```
TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
    if (root == NULL || root->val == p->val || root->val == q->val)
        return root;

    TreeNode* left = lowestCommonAncestor(root->left, p, q);
    TreeNode* right = lowestCommonAncestor(root->right, p, q);
    if (left != NULL && right != NULL) {
        return root;
    } else if (right != NULL) {
        return right;
    } else {
        return left;
    }
}
```

### 230. 二叉搜索树中第K小的元素
给定一个二叉搜索树，编写一个函数 kthSmallest 来查找其中第 k 个最小的元素。

#### 示例
```
输入: root = [3,1,4,null,2], k = 1
   3
  / \
 1   4
  \
   2
输出: 1
```
#### 思路
- 中序递归遍历，维护一个计数器，每遍历一个节点计数器减1，当计数器为0时，结束递归。
- 当计数器为0时，迭代更新的val就是第k小的值。

#### 代码
```
class Solution {
public:
    int kthSmallest(TreeNode* root, int k) {
        int val = 0;
        dfs(root, k, val);
        return val;
    }

    void dfs(TreeNode* node, int& k, int& val) {
        if (node == NULL || k <= 0)
            return;
        
        dfs(node->left, k, val);
        if (k > 0) {
            // 只有k大于0，才可以更新值
            k--;
            val = node->val;
        }
        dfs(node->right, k, val);
    }
};
```

## 树的遍历
除递归方式遍历二叉树外，另可以借助堆栈(stack)实现二叉树中序、前序、后序遍历，使用队列实现按层遍历。

迭代遍历二叉树的模板
```
while( 栈非空 || p 非空) {
    if( p 非空) {
        栈.push()     // 入栈
        print(p->val) // 先序
        p = p->left
    } else {
        p = 栈.pop()  // 出栈
        print(p->val) // 中序
        p = p.right
    }
}
```

### 94. 二叉树的中序遍历 
给定一个二叉树，返回它的中序 遍历。递归算法很简单，你可以通过迭代算法完成吗？

#### 思路
- 使用栈来完成，每次判断节点左子树是否为空，不为空则入栈，为空从栈中取出一个节点。
- 从栈中取出的节点，左子树肯定已经遍历过了，所以只需要输出val，然后迭代判断右子树。

#### 代码
```
 vector<int> inorderTraversal(TreeNode* root) {
    std::vector<int> vec_ret;
    std::stack<TreeNode*> my_stack;
    TreeNode* curr_node = root;
    while (curr_node || !my_stack.empty()) {
        if (curr_node) {
            my_stack.push(curr_node);
            curr_node = curr_node->left;
        } else {
            TreeNode* node = my_stack.top();
            my_stack.pop();
            vec_ret.push_back(node->val);    // 满足中序
            curr_node = node->right;
        }
    }
    return vec_ret;
}
```
### 144. 二叉树的前序遍历
给定一个二叉树，返回它的 前序 遍历。递归算法很简单，你可以通过迭代算法完成吗？

#### 思路
- 同上一题中序遍历类似，只不过输出节点值的时机不同

#### 代码
```
vector<int> preorderTraversal(TreeNode* root) {
    vector<int> vec_res;
    stack<TreeNode*> my_stack;
    TreeNode* curr_node = root;

    while (curr_node || !my_stack.empty()) {
        if (curr_node) {
            vec_res.push_back(curr_node->val);    // 满足先序
            my_stack.push(curr_node);
            curr_node = curr_node->left;
        } else {
            TreeNode* node = my_stack.top();
            my_stack.pop();
            curr_node = node->right;
        }
    }
    return vec_res;
}
```

### 145. 二叉树的后序遍历
给定一个二叉树，返回它的 后序 遍历。递归算法很简单，你可以通过迭代算法完成吗？

#### 思路
- 后续的难点是当前出栈的节点，不能打印（后续遍历），所以借助一个额外的栈完成。
- 由于结果是临时保存到栈里面的，所以需要先遍历右子树，再遍历左子树。

#### 代码
```
vector<int> postorderTraversal(TreeNode* root) {
    vector<int> vec_res;
    stack<int> stack_res;

    stack<TreeNode*> my_stack;
    TreeNode* curr_node = root;
    while (curr_node || !my_stack.empty()) {
        if (curr_node) {
            stack_res.push(curr_node->val);
            my_stack.push(curr_node);        // 先序保存到栈
            curr_node = curr_node->right;    // 先遍历右子树
        } else {
            TreeNode* node = my_stack.top();
            my_stack.pop();
            curr_node = node->left;        // 后遍历左子树
        }
    }
    // 将栈转为数组返回
    while (!stack_res.empty()) {
        vec_res.push_back(stack_res.top());
        stack_res.pop();
    }
    return vec_res;
}
```

### 102. 二叉树的层序遍历
给你一个二叉树，请你返回其按 层序遍历 得到的节点值。 （即逐层地，从左到右访问所有节点）。

#### 思路
- 采用队列按层级输出节点，难点在于怎么判断层级。
- 采用双层循环，第一层循环为层级间的循环，第二层循环为层级内部的循环，通过队列长度来获取当前层级节点数。

#### 代码
```
vector<vector<int>> levelOrder(TreeNode* root) {
    vector<vector<int>> result;
    if (!root) 
        return result;

    queue<TreeNode*> my_queue;
    my_queue.push(root);
    while (!my_queue.empty()) {
        int count = my_queue.size();    // 当前层级的节点数
        vector<int> tmp(count, 0);
        for (int i = 0; i < count; i++)
            TreeNode* node = my_queue.front();
            my_queue.pop();
            tmp[i] = node->val;
            if (node->left)
                my_queue.push(node->left);              
            if (node->right)
                my_queue.push(node->right);
        }
        result.push_back(tmp);
    }
    
    return result;
}
```

## 构造二叉树
给定两个序列，构造出一个二叉树，如果要构建唯一的二叉树，必须有中序列，也就是先序+中序、后序+中序。

利用先序和后序确定出根节点，然后在中序上顺序查找到根节点，分成左右两个区间，递归调用构建左右子树。

### 106. 从中序与后序遍历序列构造二叉树
根据一棵树的中序遍历与后序遍历构造二叉树，你可以假设树中没有重复的元素。

#### 示例：
```
输入：pre = [1,2,4,5,3,6,7], post = [4,5,2,6,7,3,1]
输出：[1,2,3,4,5,6,7]
```
#### 示例
```
中序遍历 inorder = [9,3,15,20,7]
后序遍历 postorder = [9,15,7,20,3]
返回如下的二叉树：

    3
   / \
  9  20
    /  \
   15   7
```

#### 思路
- 创建一个支持不同区间的递归构建函数，处理构建任务。
- 取后序的最后一个元素为根节点，并在中序序列中顺序查找，找到根节点位置，分成左右两个区间。
- 递归调用构建函数，创建并设置左右子树。

#### 代码
```
class Solution {
public:
   
    TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
        if (inorder.size() == 0 || postorder.size() == 0)
            return NULL;
        
        return build(inorder, 0, inorder.size()-1, postorder, 0, postorder.size()-1);
    }

    TreeNode* build(vector<int>& inorder, int in_s, int in_e, vector<int>& postorder, int pos_s, int pos_e) {
        if (in_s == in_e)
            return new TreeNode(inorder[in_s]);
        
        // 创建根节点
        TreeNode* root = new TreeNode(postorder[pos_e]);
        
        // 找出中序中的根节点
        int in_root = in_s;
        for (; in_root <= in_e; in_root++) {
            if (inorder[in_root] == root->val)
                break;
        }
        int len = in_root - in_s;
        if (len > 0)
            root->left = build(inorder, in_s, in_root - 1, postorder, pos_s, pos_s + len - 1);
        
        if (in_root < in_e)
            root->right = build(inorder, in_root + 1, in_e, postorder, pos_s + len, pos_e-1);

        return root;
    }
};
```

## 其他
### 114. 二叉树展开为链表
给定一个二叉树，原地将它展开为一个单链表。

#### 示例
```
给定二叉树

    1
   / \
  2   5
 / \   \
3   4   6

将其展开为：
1
 \
  2
   \
    3
     \
      4
       \
        5
         \
          6
```

#### 思路
- 对二叉树后序遍历，设定一个 last 前置节点，将当前节点指向前置节点。

#### 代码
```
TreeNode* last = nullptr;
void flatten(TreeNode* root) {
    if (root == nullptr) return;
    flatten(root->right);
    flatten(root->left);
    root->right = last;
    root->left = nullptr;
    last = root;
}
```
# 查找
## 二分查找
二分查找也叫折半查找，用于在有序序列中实现 O(logn )的查找，实现思路是每次取中间值判断大小，然后将查找范围缩小到前一步或者后一半。

### 704. 二分查找
给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target，写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。

#### 思路
- 如果序列为偶数，则取中间两个元素的前一个元素。
- 收缩范围时，要取中间元素的下一个元素，m-1或者m+1，否则会死循环。

#### 代码
```
int search(vector<int>& nums, int target) {
    int l = 0, r = nums.size() - 1;
    while (l <= r) {
        int m = (l+r)/2;
        if (nums[m] > target) {
            r = m-1;
        } else if (nums[m] < target) {
            l = m+1;
        } else {
            return m;
        }
    }
    return -1;
}
```

### 34. 在排序数组中查找元素的第一个和最后一个位置
给定一个按照升序排列的整数数组 nums，和一个目标值 target。找出给定目标值在数组中的开始位置和结束位置。

你的算法时间复杂度必须是 O(log n) 级别。

如果数组中不存在目标值，返回 [-1, -1]。

#### 示例
```
输入: nums = [5,7,7,8,8,10], target = 8
输出: [3,4]
示例 2:

输入: nums = [5,7,7,8,8,10], target = 6
输出: [-1,-1]
```

#### 思路
- 二分查找，先查左边界，再查出右边界。
- 这里要注意去中间值默认的偶数的前一个值，所以查左边界时，如果中间值等于目标值，则收缩时不需要-1。
- 查右边界时，中间值需要取偶数的后一个值，这样查右边界时，如果中间值等于目标值，则收缩时不需要+1。

#### 代码
```
vector<int> searchRange(vector<int>& nums, int target) {
    vector<int> vec_ret;
    int bpos = -1, epos = -1, mpos = -1;
    
    // 先查左边界
    int l = 0, r = nums.size() - 1;
    while (l < r) {
        int m = (l+r)/2;
        if (nums[m] > target)
            r = m -1;
        else if (nums[m] < target)
            l = m + 1;
        else {
            r = m;
        }
    }

    // 左边界找到，则找右边界
    if (l == r && nums[l] == target) {
        bpos = l;
        
        // 查找右边界
        r = nums.size() - 1;
        while (l < r) {
            int m = (l+r)/2 + 1;
            if (nums[m] > target)
                r = m - 1;
            else
                l = m;
        }
        epos = l;
    }
        
    vec_ret.push_back(bpos);
    vec_ret.push_back(epos);
    return vec_ret;
}
```
# 数组
## 概述 
数组是连续存储，可以在 O(1) 时间读取任意下标元素，经常应用于排序(Sort)、双指针(Two Pointers)、二分查找(Binary Search)、动态规划(DP)等算法。

顺序访问数组、按下标取值是对数组的常见操作。

## 数组遍历

### 485. 最大连续1的个数
给定一个二进制数组， 计算其中最大连续1的个数。

#### 示例
```
输入: [1,1,0,1,1,1]
输出: 3
解释: 开头的两位和最后的三位都是连续1，所以最大连续1的个数是 3.
```

#### 思路
- 维护一个最大值计数器，和一个迭代计数器，遍历数组，如果为1，则迭代计数器+1，并更新最大值计数器。
- 如果元素值为0，则迭代计数器清零。

#### 代码
```
int findMaxConsecutiveOnes(vector<int>& nums) {
    int max_count = 0, count = 0;
    for (auto& v : nums) {
        if (v == 0) {
	    count = 0;
	} else {
	    count++;
	    max_count = max(max_count, count);
	}
    }
    return max_count;
}
```

### 414. 第三大的数
给定一个非空数组，返回此数组中第三大的数。如果不存在，则返回数组中最大的数。要求算法时间复杂度必须是O(n)。

#### 示例
```
输入: [2, 2, 3, 1]

输出: 1

解释: 注意，要求返回第三大的数，是指第三大且唯一出现的数。
存在两个值为2的数，它们都排第二
```
#### 思路
- 维护三个最大值的下标，然后循环遍历更新这三个值。
- 注意不要使用INT_MIN，因为INT_MIN也可能是数组的元素，会造成返回的时候误判。

#### 代码
```
int thirdMax(vector<int>& nums) {
    int p1 = 0, p2 = -1, p3 = -1;
    for (int i = 0; i < nums.size(); i++) {
        if (nums[i] > nums[p1]) {
            if (p2 == -1)
                p2 = p1;
            else {
                p3 = p2;
                p2 = p1;
            }
            p1 = i;
        } else if (nums[i] < nums[p1]) {
            if (p2 == -1)
                p2 = i;
            else if (nums[i] > nums[p2]) {
                p3 = p2;
                p2 = i;
            } else if (nums[i] < nums[p2]) {
                if (p3 == -1)
                    p3 = i;
                else if (nums[i] > nums[p3])
                    p3 = i;
            }
        }
    }
    return p3 == -1 ? nums[p1] : nums[p3];
}
```

### 169. 多数元素
给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。

#### 示例
```
输入: [2,2,1,1,1,2,2]
输出: 2
```

#### 思路
- 摩尔投票法：核心就是对拼消耗,最后剩下的一定是个数最多那个。
- 维持一个计数器，遇到相等的就+1，遇到不同的就减1，计数器为0时，将基准数换成下一个数，最后剩下的基准数就是个数最多那个。

#### 代码
```
int majorityElement(vector<int>& nums) {
    int count = 0;
    int tmp = 0;
    for (int i = 0; i < nums.size(); i++) {
        if (count == 0) {
            tmp = nums[i];
            count++;
        } else {
            if (nums[i] == tmp)
                count++;
            else
                count--;
        }
    }
    return tmp;
}
``` 

### 88. 合并两个有序数组
给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。
- 初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。
- 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素

#### 示例
```
输入:
nums1 = [1,2,3,0,0,0], m = 3
nums2 = [2,5,6],       n = 3

输出: [1,2,2,3,5,6]
```

#### 思路
- 在nums1上维护一个从后往前的指针，从后往前归并排序，填充到num1上。

#### 代码
```
void merge(vector<int>& nums1, int m, vector<int>& nums2, int n) {
    int last = m + n - 1;
    while (n) {
        if (m == 0) {
            nums1[last--] = nums2[--n];
        } else if (nums2[n-1] > nums1[m-1]) {
            nums1[last--] = nums2[--n];
        } else {
            nums1[last--] = nums1[--m];
        }
    }
}
```

## 双指针

### 26. 删除排序数组中的重复项
给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。

#### 示例
```
给定 nums = [0,0,1,1,1,2,2,3,3,4],

函数应该返回新的长度 5, 并且原数组 nums 的前五个元素被修改为 0, 1, 2, 3, 4。

你不需要考虑数组中超出新长度后面的元素。
```

#### 思路
- 采用双指针，左指针维护不重复序列，右指针往右移动，判断是否重复

#### 代码
```
 int removeDuplicates(vector<int>& nums) {
    if (nums.size() < 2)
        return nums.size();

    int l = 0, r = 0;
    for (; r < nums.size(); r++) {
        if (nums[r] > nums[l]) {
            nums[++l] = nums[r];
        }
    }

    return l+1;
}
```

### 905. 按奇偶排序数组
给定一个非负整数数组 A，返回一个数组，在该数组中， A 的所有偶数元素之后跟着所有奇数元素。

#### 示例
```
输入：[3,1,2,4]
输出：[2,4,3,1]
输出 [4,2,3,1]，[2,4,1,3] 和 [4,2,1,3] 也会被接受。
```

#### 思路
- 采用双指针，从右寻找偶数，从左寻找奇数，交换偶数和奇数的位置。
- 时间复杂度 O(n),空间复杂度O(1)。

#### 代码
```
vector<int> sortArrayByParity(vector<int>& A) {
    int l = 0, r = A.size()-1;
	
    while (l < r) {
        while (l < r && A[r]%2 == 1)
            r--;
        while (l < r && A[l]%2 == 0)
            l++;
        swap(A[l++], A[r--]);
    }
    
    return A;
}
```

### 922. 按奇偶排序数组 II
给定一个非负整数数组 A， A 中一半整数是奇数，一半整数是偶数。

对数组进行排序，以便当 A[i] 为奇数时，i 也是奇数；当 A[i] 为偶数时， i 也是偶数。

#### 示例
```
输入：[4,2,5,7]
输出：[4,5,2,7]
解释：[4,7,2,5]，[2,5,4,7]，[2,7,4,5] 也会被接受。
```
#### 思路
- 采用双指针，一个指向偶数位，一个指向奇数位，每次迭代移动两位。
- 各自循环找到一个不符合要求的数字，然后互相交换。 

#### 代码
```
vector<int> sortArrayByParityII(vector<int>& A) {
    int i = 0, j = 1;
    while (i < A.size() && j < A.size()) {
        while (i < A.size() && A[i]%2 == 0)
            i += 2;
        while (j < A.size() && A[j]%2 == 1)
            j += 2;
            
        if (i < A.size() && j < A.size())
            swap(A[i], A[j]);
    }
    return A;
}
```

### 977. 有序数组的平方
给定一个按非递减顺序排序的整数数组 A，返回每个数字的平方组成的新数组，要求也按非递减顺序排序。

#### 示例
```
输入：[-7,-3,2,3,11]
输出：[4,9,9,49,121]
```

#### 思路
- 双指针，从两端进行比较，平方值大的那个一定是数列中的最大值。
- resize事先分配好返回的vector，从后往前填充结果。

#### 代码
```
vector<int> sortedSquares(vector<int>& A) {
    // 双指针比较平方大小，从后往前填充数组
    vector<int> vec_ret;
    vec_ret.resize(A.size());

    int i = 0, j = A.size() - 1;
    int k = j;
    while (i <= j) {
        if (A[i]*A[i] >= A[j]*A[j])
            vec_ret[k--] = A[i]*A[i++];
        else
            vec_ret[k--] = A[j]*A[j--];
    }

    return vec_ret;
}
```

## 二维数组
### 867. 转置矩阵
给定一个矩阵 A， 返回 A 的转置矩阵。

矩阵的转置是指将矩阵的主对角线翻转，交换矩阵的行索引与列索引。

#### 示例
```
输入：[[1,2,3],[4,5,6],[7,8,9]]
输出：[[1,4,7],[2,5,8],[3,6,9]]

输入：[[1,2,3],[4,5,6]]
输出：[[1,4],[2,5],[3,6]]
```

#### 思路
- 先初始化一个和转置矩阵形状一样的二维数组B。
- 遍历原矩阵A，将行当列，列当行去设置转置矩阵B。

#### 代码
```
vector<vector<int>> transpose(vector<vector<int>>& A) {
    int rows = A.size();
    int cols = A[0].size();
    
    // 初始化转置矩阵
    vector<vector<int>> B;
    B.resize(cols);
    for (auto &t : B) {
        t.resize(rows);
    }
    
    // 转置填充
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < cols; j++) {
            B[j][i] = A[i][j];
        }
    }
    return B;
}
```

### 54. 螺旋矩阵
给定一个包含 m x n 个元素的矩阵（m 行, n 列），请按照顺时针螺旋顺序，返回矩阵中的所有元素。

#### 示例
```
输入:
[
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9,10,11,12]
]
输出: [1,2,3,4,8,12,11,10,9,5,6,7]
```
#### 思路
- 设定四个边界，在边界条件内进行循环。
- 每次循环一次，收缩边界。

#### 代码
```
vector<int> spiralOrder(vector<vector<int>>& matrix) {
    vector<int> result;
    if (matrix.size() == 0 || matrix[0].size() == 0)
        return result;
	
    int rows = matrix.size();
    int cols = matrix[0].size();
    int left = 0, right = cols -1, top = 0, bottom = rows -1;
    while (left <= right && top <= bottom) {
        // 从左到右
        for (int i = left; i <= right; i++)
            result.push_back(matrix[top][i]);

        // 从上到下
        for (int i = top+1; i <= bottom; i++)
            result.push_back(matrix[i][right]);

        // 从右到左
        if(top != bottom) {
            for(int i = right - 1; i >= left; --i)
                result.push_back(matrix[bottom][i]);
        }
        // 从下到上
        if(left != right) {
            for(int i = bottom - 1; i > top; --i)
                result.push_back(matrix[i][left]);
        }
        left++, top++, right--, bottom--;
    }
    return result;
}
```

### 面试题 01.07. 旋转矩阵
有一个NxN整数矩阵，请编写一个算法，将矩阵顺时针旋转90度。不占用额外内存空间能否做到？

#### 示例
```
给定 matrix = 
[
  [1,2,3],
  [4,5,6],
  [7,8,9]
],

原地旋转输入矩阵，使其变为:
[
  [7,4,1],
  [8,5,2],
  [9,6,3]
]
```

#### 思路
- 先对矩阵进行转置，由[[1,2,3],[4,5,6],[7,8,9]]得到[[1,4,7],[2,5,8],[3,6,9]]
- 再遍历每一行，对每行首尾进行翻转

#### 代码
```
void rotate(vector<vector<int>>& matrix) {
    // 先对矩阵进行转置
    int size = matrix.size();
    for (int i = 0; i < size; i++) {
        for (int j = i; j < size; j++)
            swap(matrix[i][j], matrix[j][i]);
    }

    // 每一行左右翻转
    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size/2; j++)
            swap(matrix[i][j], matrix[i][size-1-j]);
    }
}
```
# 链表
## 概述
链表在物理存储上不连续、内存管理比较灵活，插入和删除满足  O(1) 时间复杂度，但不支持按索引存取，查找时需要顺序查找。

### 链表的定义
```
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};
```

## 链表的删除

### 237. 删除链表中的节点
请编写一个函数，使其可以删除某个链表中给定的（非末尾）节点。传入函数的唯一参数为 要被删除的节点。

#### 思路
- 由于获取不到前驱节点，所以没有办法通过调整指针指向的方法删除节点。
- 可以将下一个节点复制到当前节点上，然后删除下一个节点。

#### 代码
```
void deleteNode(ListNode* node) {
    ListNode* next = node->next;
    *node = *next;
    delete next;
}
```

### 203. 移除链表元素
删除链表中等于给定值 val 的所有节点。

#### 示例
```
输入: 1->2->6->3->4->5->6, val = 6
输出: 1->2->3->4->5
```
#### 思路
- 这道题的难点是如果第一个节点就是待删除节点，处理起来不好处理，这里创建一个虚节点指向头节点。 
- 循环迭代节点，注意不要迭代判断当前节点是否需要删除，而是判断当前节点的下一个节点是否需要删除。

#### 代码
```
ListNode* removeElements(ListNode* head, int val) {
    ListNode* node = new ListNode(-1);    // 创建虚节点
    node->next = head;
    head = node;
    
    while (node->next) {
        ListNode* next_node = node->next;
        if (next_node->val == val) {    // 判断当前节点的下一个节点是否需要删除
            node->next = next_node->next;
            delete next_node;
        } else {
            node = next_node;
        }
    }
    return head->next;
}
```

### 83. 删除排序链表中的重复元素
给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。

#### 示例
```
输入: 1->1->2->3->3
输出: 1->2->3
```

#### 思路
- 循环迭代链表，循环条件为当前节点的下一个节点不为空。
- 判断当前节点和下一个节点是否相等，如果相等则指向下下个节点，继续循环判断下一个节点。
- 如果不等，迭代当前节点。

#### 代码
```
ListNode* deleteDuplicates(ListNode* head) {
    ListNode* node = head;
    while (node && node->next) {
        if (node->val == node->next->val) {
            node->next = node->next->next;    // 重复，删除下一个节点，指向下下个节点
        } else {
            node = node->next;                // 不重复，迭代当前节点
        }
    }
    return head;
}
```

### 82. 删除排序链表中的重复元素 II
给定一个排序链表，删除所有含有重复数字的节点，只保留原始链表中 没有重复出现 的数字

#### 示例
```
示例 1:

输入: 1->2->3->3->4->4->5
输出: 1->2->5

示例 2:

输入: 1->1->1->2->3
输出: 2->3
```

#### 思路
- 创建一个虚节点，维护一个正确链表，然后循环判定每个节点是否是非重复节点，如果是添加到正确链表中。
- 判定非重复节点时，需要维护一个前驱节点值，用于判定和前驱节点是否重复。
- 结束时，需要将正确链表的末尾置为 NULL。

#### 代码
```
ListNode* deleteDuplicates(ListNode* head) {
    if (!head || !head->next)
        return head;

    // 创建一个虚节点
    ListNode* dummy = new ListNode(-1);
    ListNode* tail = dummy;	// tail用来维护一个正确链表

    int front = head->val + 1;	// 初始化一个前驱节点值
    while (head) {
        // 判断是否合法
        if (head->val != front) {
            if (!head->next || head->val != head->next->val) {
                tail->next = head;
                tail = head;
            }
            
            // 更新前驱
            front = head->val;
        }
        head = head->next;
    }
    
    tail->next = NULL;
    return dummy->next;
}
```

### 19. 删除链表的倒数第N个节点
给定一个链表，删除链表的倒数第 n 个节点，并且返回链表的头结点。

#### 示例
```
给定一个链表: 1->2->3->4->5, 和 n = 2.

当删除了倒数第二个节点后，链表变为 1->2->3->5.
```

#### 思路
- 采用递归的方式找到待删除节点的上一个节点，进行删除
- 维护一个计数器，每次递归一个节点返回时，计数器减1，当计数器为-1时，说明上一个节点是待删除节点
- 第一个节点也有可能被删除，所以需要借助虚节点

#### 代码
```
class Solution {
public:
    ListNode* removeNthFromEnd(ListNode* head, int n) {
        if (!head || n == 0)
		    return head;	

        // 定义一个虚节点
	    ListNode* dummy = new ListNode(-1);
	    dummy->next = head;

        count = n;
	    remove(dummy);
	    return dummy->next;
    }

private:
    void remove(ListNode* node) {
        if (!node)
            return;
        
        // 递归下一个节点
        remove(node->next);
        count--;

        // count 为 -1，说明下一个节点就是目标节点 
        if (count == -1) {
            node->next = node->next->next;		
        }
    }

    int count;
};
```

## 链表的排列
链表的排列一般都是通过修改next指针指向实现。

### 143. 重排链表
给定一个单链表 L：L0→L1→…→Ln-1→Ln ，将其重新排列后变为： L0→Ln→L1→Ln-1→L2→Ln-2→…

#### 示例
```
给定链表 1->2->3->4->5, 重新排列为 1->5->2->4->3.
```

#### 思路
- 找到中间节点，将链表分为前后两半部分
- 对后半部分链表进行逆序
- 合并前后两半部分链表

#### 代码
```
 void reorderList(ListNode* head) {
    if (!head || !head->next)
        return;
    
    // 寻找中间节点的（偶数时取前一个中间节点）
    ListNode* slow = head;
    ListNode* fast = head->next;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }

    // 将链表分成前后两部分
    ListNode* head2 = slow->next;
    slow->next = nullptr;

    // 将head2逆序
    ListNode* front = nullptr;
    while (head2) {
        ListNode* next = head2->next;
        head2->next = front;
        front = head2;
        head2 = next;
    }
    head2 = front;

    // 归并
    ListNode* head1 = head;
    while (head1 && head2) {
        ListNode* next = head1->next;
        head1->next = head2;
        head1 = next;

        // 由于head1长度大于等于head2，所以不会出现head1为null，head2不为null的情况
        next = head2->next;
        head2->next = head1;
        head2 = next;
    }
}
```

### 86. 分隔链表
给定一个链表和一个特定值 x，对链表进行分隔，使得所有小于 x 的节点都在大于或等于 x 的节点之前。

你应当保留两个分区中每个节点的初始相对位置。

#### 示例
```
输入: head = 1->4->3->2->5->2, x = 3
输出: 1->2->2->4->3->5
```

#### 思路
- 创建两个虚节点，维护两个分区链表，一个小于x，一个大于等于x。
- 遍历链表，设置两个分区链表，最后将两个链表拼接起来。

#### 代码
```
ListNode* partition(ListNode* head, int x) {
    ListNode* part1 = new ListNode(-1);
    ListNode* node1 = part1;
    ListNode* part2 = new ListNode(-1);
    ListNode* node2 = part2;
    
    while (head) {
        if (head->val < x) {
            node1->next = head;
            node1 = node1->next;
        } else {
            node2->next = head;
            node2 = node2->next;
        }
        head = head->next;
    }
    
    // 连接两个分区
    node1->next = part2->next;
    node2->next = NULL;
    return part1->next;
}
```

### 328. 奇偶链表
给定一个单链表，把所有的奇数节点和偶数节点分别排在一起。请注意，这里的奇数节点和偶数节点指的是节点编号的奇偶性，而不是节点的值的奇偶性。

请尝试使用原地算法完成。你的算法的空间复杂度应为 O(1)，时间复杂度为O(n).

#### 示例
```
输入: 1->2->3->4->5->NULL
输出: 1->3->5->2->4->NULL

输入: 2->1->3->5->6->4->7->NULL 
输出: 2->3->6->7->1->5->4->NULL
```
#### 思路
- 和上一题思路一样，通过创建两个虚节点，分别维护一个奇数链表和一个偶数链表，最后合并。

#### 代码
```
ListNode* oddEvenList(ListNode* head) {
    ListNode* head1 = new ListNode(-1);
    ListNode* head2 = new ListNode(-1);
    ListNode* node1 = head1;
    ListNode* node2 = head2;
    
    int i = 0;
    while (head) {
        i++;
        if (i%2) {
            node1->next = head;
            node1 = head;
        } else {
            node2->next = head;
            node2 = head;
        }
        head = head->next;
    }
    
    node1->next = head2->next;
    node2->next = NULL;
    return head1->next;
}
```

### 206. 反转链表
反转一个单链表。

#### 示例
```
输入: 1->2->3->4->5->NULL
输出: 5->4->3->2->1->NULL
```
#### 思路
- 分为递归和循环迭代两种解法，这里只说循环迭代。
- 维护一个前驱指针，循环节点，修改指向。最后返回这个前驱指针。

#### 代码
常规解法：
```
ListNode* reverseList(ListNode* head) {
    ListNode* pre_node = NULL;
    
    while (head) {
        ListNode* next = head->next;
        head->next = pre_node;
        pre_node = head;
        head = next;
    }
    return pre_node;
}

递归解法：
ListNode* reverseList(ListNode* head) {
    if (head == NULL || head->next == NULL)
        return head;
            
    ListNode* new_head = reverseList(head->next);
    head->next->next = head;
    head->next = NULL;
    return new_head;
}
```

### 92. 反转链表 II
反转从位置 m 到 n 的链表。请使用一趟扫描完成反转。

#### 示例
```
输入: 1->2->3->4->5->NULL, m = 2, n = 4
输出: 1->4->3->2->5->NULL
```

#### 思路
- 顺序遍历链表，遇到m和n区间节点，进行逆序操作。
- 本题关键是要记录区间的开始节点和结束节点，用于逆序后的合并
- 有可能从第一个节点开始翻转，所以需要创建一个虚节点

#### 代码
```
ListNode* reverseBetween(ListNode* head, int m, int n) {
    // 创建一个虚节点
    ListNode* dummy = new ListNode(-1);
    dummy->next = head;
    ListNode* tail = dummy;

    ListNode* end = NULL;
    ListNode* front = NULL;
    for (int i = 1; i <= n; i++) {
        if (i < m) {
            tail->next = head;
            tail = head;
            head = head->next;
        } else if (i == m) {
            end = head;
            front = head;
            head = head->next;
        } else {
            ListNode* next = head->next;
            head->next = front;
            front = head;
            if (i == n) {
                // 区间末尾节点
                tail->next = head;
                end->next = next; // 逆序后的尾节点指向n后边的链表
            }
            head = next;    
        }
    }
    return dummy->next;
}
```

### 61. 旋转链表
给定一个链表，旋转链表，将链表每个节点向右移动 k 个位置，其中 k 是非负数。

#### 示例
```
示例 1:

输入: 1->2->3->4->5->NULL, k = 2
输出: 4->5->1->2->3->NULL
解释:
向右旋转 1 步: 5->1->2->3->4->NULL
向右旋转 2 步: 4->5->1->2->3->NULL
示例 2:

输入: 0->1->2->NULL, k = 4
输出: 2->0->1->NULL
解释:
向右旋转 1 步: 2->0->1->NULL
向右旋转 2 步: 1->2->0->NULL
向右旋转 3 步: 0->1->2->NULL
向右旋转 4 步: 2->0->1->NULL
```
#### 思路
- 找到倒数第 k 个节点，作为新的开始节点，将原链表的首尾相接，并将新开始节点的前驱节点指向NULL，作为新的末尾节点。
- k 如果等于链表长度，则不需要调整直接返回原链表，如果 k 大于链表长度，则对链表长度求余。

#### 代码
```
ListNode* rotateRight(ListNode* head, int k) {
    if (!head || !head->next)
        return head;
        
    // 寻找末尾节点，并计算链表长度
    int len = 1;
    ListNode* end_node = head;
    while (end_node->next) {
        len++;
        end_node = end_node->next;
    }
    
    // 计算旋转后链表的开头位置
    int head_pos = len - k;
    if (head_pos == 0)
        return head;
    else if (head_pos < 0)
        head_pos = len - k%len;
    
    // 寻找新开始节点的前一个节点
    ListNode* pre_node = head;
    for (int i = 1; i < head_pos; i++) {
        pre_node = pre_node->next;
    }
    
    // 将末尾节点指向head节点
    end_node->next = head;
    
    // 获取新的开始节点
    head = pre_node->next;
    
    // 新开始节点的前一个节点指向NULL
    pre_node->next = NULL;
    
    return head;
}
```

## 快慢指针
快慢指针用于链表的环形查找，或者查找中间节点，代码框架
```
ListNode* slow = head;
    ListNode* fast = head;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
```
- 快慢指针初始化时从head节点开始，当节点为偶数时，慢节点指向第二个中间节点。
- 如果想让慢指针最后指向前一个中间节点，则fast需要从head->next开始，先走一步。
- 如果想让慢指针最后指向中间节点(不管奇偶)的前一个节点，则fast需要从head->next->next开始,先走两步。
- 环形链表中，快指针有可能会跨过慢指针，但是下一次循环一定还会相遇。

### 876. 链表的中间结点
给定一个带有头结点 head 的非空单链表，返回链表的中间结点。

如果有两个中间结点，则返回第二个中间结点。

#### 示例
```
输入：[1,2,3,4,5,6]
输出：此列表中的结点 4 (序列化形式：[4,5,6])
由于该列表有两个中间结点，值分别为 3 和 4，我们返回第二个结点。
```
#### 思路
- 使用快慢指针，快慢指针都从head节点开始，快指针走两步，慢指针走一步。
- 奇数个节点时，快指针在结尾时能够走完两步，这个时候慢指针指向的就是中间节点。
- 偶数个节点时，快指针在结尾时只能走一步，这个时候慢指针指向的就是第二个中间节点。

#### 代码
```
ListNode* middleNode(ListNode* head) {
    ListNode* fast = head; // 如果要取第一个中间节点，则需要fast = head->next
    ListNode* slow = head;
    
    while (fast && fast->next) {    // 快指针走一步（偶数）或者两步（奇数），慢指针走一步
        slow = slow->next;
        fast = fast->next->next;
    }
    return slow;
}
```

141. 环形链表
给定一个链表，判断链表中是否有环。

#### 思路
- 采用快慢指针，按照步调往前走，两个节点一定会重复，有可能快指针会跨过慢指针，但是下一次追上一定会相遇

#### 代码
```
bool hasCycle(ListNode *head) {
    if (!head || !head->next)
        return false;
    
    ListNode* slow = head;
    ListNode* fast = head;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
        if (slow == fast)
            return true;
    }
    return false;
}
```

### 142. 环形链表 II
给定一个链表，返回链表开始入环的第一个节点。 如果链表无环，则返回 null。

#### 思路
- 假设直线段长 l，环长 c，追上时慢指针在环内走了 r，即慢指针一共走了 l+r，快指针走了 2l+2r。
- 快指针追上时多走了 n(n>=1) 个环长，所以有 2l+2r-(l+r)=nc，也就是 l+r=nc，则得到公式：l=(n-1)c+(c-r)。
- 由公式可知，如果两个慢指针分别同时从头结点和追遇节点开始走，那么二者在环入口相遇时都走了l。

```
ListNode *detectCycle(ListNode *head) {
   if (!head || !head->next)
        return NULL;
    
    // 定义快慢指针寻找相交节点
    ListNode* slow = head;
    ListNode* fast = head;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;

        if (slow == fast) {
            // head节点和相交节点同时出发，相遇时的节点就是环
            while (head != slow) {
                head = head->next;
                slow = slow->next;
            }
            return slow;
        }
    }
    
    return NULL;
}
```
### 109. 有序链表转换二叉搜索树
给定一个单链表，其中的元素按升序排序，将其转换为高度平衡的二叉搜索树。

#### 示例
```
给定的有序链表： [-10, -3, 0, 5, 9],

一个可能的答案是：[0, -3, 9, -10, null, 5], 它可以表示下面这个高度平衡二叉搜索树：

      0
     / \
   -3   9
   /   /
 -10  5
```

#### 思路
- 通过快慢指针，寻找到中间节点的前一个节点，然后将链表分割为前后两个链表
- 递归构建二叉树

#### 代码
```
 TreeNode* sortedListToBST(ListNode* head) {
    if (!head)
        return NULL;

    if (!head->next)
        return new TreeNode(head->val);
    
    // 通过快慢指针寻找中间节点的前一个节点
    ListNode* slow = head;
    ListNode* fast = head->next->next;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
    TreeNode* root = new TreeNode(slow->next->val);
    root->right = sortedListToBST(slow->next->next);
    slow->next = NULL;  // 分割链表
    root->left = sortedListToBST(head);

    return root;
}
```

## 多链表处理
多链表的增删改、合并，与单链表的处理方式一样，只是增加了对多个链表的遍历。

### 21. 合并两个有序链表
将两个升序链表合并为一个新的升序链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。 

#### 示例
```
输入：1->2->4, 1->3->4
输出：1->1->2->3->4->4
```
#### 思路
- 通过递归来实现

#### 代码
```
ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    if (l1 == NULL)
        return l2;
    
    if (l2 == NULL)
        return l1;

    if (l1->val < l2->val) {
        l1->next = mergeTwoLists(l1->next, l2);
        return l1;
    } else {
        l2->next = mergeTwoLists(l1, l2->next);
        return l2;
    }
}
```

### 23. 合并K个升序链表
给你一个链表数组，每个链表都已经按升序排列。

请你将所有链表合并到一个升序链表中，返回合并后的链表。

#### 示例
```
输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6
```
#### 思路
- 顺序合并：每次遍历各个链表，找到最小的那个节点，链接到正确链表上，并将vector中的值设置为下一个节点。
- 分治法：N 个链表合并，可以转成递归合并 N/2 个链表，最终转成求两个链表的合并。

#### 代码（顺序合并）
```
ListNode* mergeKLists(vector<ListNode*>& lists) {
	ListNode* dummy_node = new ListNode(-1);
	ListNode* node = dummy_node;
	
	while (true) {
		ListNode* min_node = NULL;
		int min_pos = -1;
		
		// 寻找当前最小的节点
		for (int i = 0; i < lists.size(); i++) {
			if (!lists[i])
				continue;
			
			if (min_node == NULL || lists[i]->val < min_node->val) {
				min_node = lists[i];
				min_pos = i;
			}
		}
		
		// 没找则说明所有链表都被处理
		if (!min_node)
			break;
			
		// 将当前节点执行它的下一个节点
		lists[min_pos] = min_node->next;
		min_node->next = NULL;
		
		node->next = min_node;
		node = node->next;
	}
	
	return dummy_node->next;
}
```

#### 代码（分治法）
```
 ListNode* mergeTwoLists(ListNode *a, ListNode *b) {
    if ((!a) || (!b)) return a ? a : b;
    ListNode head, *tail = &head, *aPtr = a, *bPtr = b;
    while (aPtr && bPtr) {
        if (aPtr->val < bPtr->val) {
            tail->next = aPtr; aPtr = aPtr->next;
        } else {
            tail->next = bPtr; bPtr = bPtr->next;
        }
        tail = tail->next;
    }
    tail->next = (aPtr ? aPtr : bPtr);
    return head.next;
}

ListNode* merge(vector <ListNode*> &lists, int l, int r) {
    if (l == r) return lists[l];
    if (l > r) return nullptr;
    int mid = (l + r) >> 1;
    return mergeTwoLists(merge(lists, l, mid), merge(lists, mid + 1, r));
}

ListNode* mergeKLists(vector<ListNode*>& lists) {
    return merge(lists, 0, lists.size() - 1);
}
```

## 链表排序
对链表进行排序一般指原址排序，即修改节点指针指向、而不修改节点的值。对链表进行归并排序(Merge Sort)，平均时间复杂度为O(nlogn)，相比其他排序方法，归并排序在平均时间复杂度上是较优的方法。

### 148. 排序链表
在 O(n log n) 时间复杂度和常数级空间复杂度下，对链表进行排序。

#### 思路
- 采用归并排序。
- 通过快慢指针寻找中间节点，对链表进行二分。

#### 代码
```
ListNode* sortList(ListNode* head) {
    if (!head || !head->next)
        return head;
        
    // 通过快慢指针寻找中间节点，这里要保证如果只有两个节点，可以被分开
    ListNode* slow = head;
    ListNode* fast = head->next;
    while (fast && fast->next) {
        slow = slow->next;
        fast = fast->next->next;
    }
    
    ListNode* head2 = slow->next;
    slow->next = NULL;
    
    return mergeList(sortList(head), sortList(head2));
}

ListNode* mergeList(ListNode* l1, ListNode* l2) {
    if (!l1)
        return l2;
    if (!l2)
        return l1;
    
    if (l1->val < l2->val) {
        l1->next = mergeList(l1->next, l2);
        return l1;
    } else {
        l2->next = mergeList(l2->next, l1);
        return l2;
    }
}
```


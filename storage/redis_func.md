# 基于 redis 的设计

## 查找附近的人
### 概述
Redis 3.2 中增加了对 GEO（Geographic 地理信息）类型的支持，redis基于该类型，提供了经纬度设置，查询，范围查询，距离查询，经纬度Hash等常见操作。

组合使用 GEOADD 和 GEORADIUS 可实现“附近的人”中“增”和“查”的基本功能。

### 方法
#### GEOADD
添加成员的经纬度信息
##### 语法
```
GEOADD key 经度 纬度 member
```
##### 示例
```
geoadd citys 125.19 43.54 changchun
geoadd citys 122.50 45.38 baicheng
geoadd citys 126.26 41.56 baishan
geoadd citys 124.18 45.30 daan
geoadd citys 125.42 44.32 dehui 
```
#### GEODIST
计算成员间距离
##### 语法
```
GEODIST key member1 member2 [unit]
```
unit 为结果单位，可选，支持：m，km，mi，ft，分别表示米（默认），千米，英里，英尺。
##### 示例
```
>  GEODIST citys changchun dunhua
"240309.2820"

>  GEODIST citys changchun dunhua km
"240.3093"
```
#### GEORADIUS 
基于经纬度坐标的范围查询
##### 语法
```
GEORADIUS key longitude latitude radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [COUNT count] [ASC|DESC] [STORE key] [STOREDIST key]
```
检索以某个经纬度为圆心，在特定半径的圆形范围内的成员。
- WITHCOORD，同时获取成员经纬度
- WITHDIST，同时获取距离参考点（圆心）的距离
- WITHHASH，同时获取成员经纬度HASH值
- COUNT count，限制获取成员的数量
- ASC|DESC，结果升降序排序
- STORE key，在命令表，READONLY模式下使用
- STOREDIST key，在命令表，READONLY模式下使用

##### 示例
```
> GEORADIUS citys 125 42 100 km
1) "tonghua"
2) "meihekou"
3) "liaoyuan"
```
#### GEORADIUSBYMEMBER 
基于成员位置范围查询

##### 语法
```
GEORADIUSBYMEMBER key member radius m|km|ft|mi [WITHCOORD] [WITHDIST] [WITHHASH] [COUNT count] [ASC|DESC] [STORE key] [STOREDIST ke##y]
```
##### 示例
```
> GEORADIUSBYMEMBER citys changchun 100 km
1) "siping"
2) "gongzhuling"
3) "changchun"
4) "jilin"
5) "jiutai"
6) "dehui"
```
#### GEOHASH 
计算经纬度 Hash，是表示坐标的一种方法，便于检索，存储。

##### 语法
```
GEOHASH key member [member ...]
```
##### 示例
```
> GEOHASH citys changchun
1) "wz9p8y0wfk0"
```

### 原理分析
#### GEOADD 原理
内部使用有序集合(zset)保存位置对象，元素的 score 值为其经纬度对应的52位的 geohash 值。

geoadd过程：
- 参数提取和校验；
- 将入参经纬度转换为52位的geohash值（score）；
- 调用 ZADD 命令将 member 及其对应的 score 存入集合 key 中。

##### gohash原理
geohash 的思想是将二维的经纬度转换成一维的字符串，并以 base32 的方式编码。
- 利用 52bit 可以存储10位的geohash值，对应地理区域大小为0.6*0.6米的格子。
- geohash 计算的字符串，可以反向解码出原来的经纬度。
- 字符串相似的表示距离相近，利用字符串的前缀匹配，可以查询附近的地理位置。

#### GEORADIUS 原理
给一个经纬度和距离，返回方圆内附近的元素。

实现逻辑：
- 参数提取和校验；
- 利用中心点和输入半径计算待查区域范围，得到一个九宫格。
- 对九宫格进行遍历，根据每个 geohash 网格的范围框选出位置对象。进一步找出与中心点距离小于输入半径的对象，进行返回。

## 分布式锁
### 概述
redis 使用 SETNX 命令来实现分布式锁，该命令只有当key不存在时才执行成功并返回1，否则不执行并返回 0。

调用SETNX返回0时，说明key存在，资源被其他服务占用，此时需要等待自旋，并不断重试，调用SETNX返回1时，说明设置成功了，资源加锁成功，使用完成后要记得删除，最保险的办法是设置key过期时间。

如果SETNX调用成功后，程序crash掉，导致key没有办法设置过期时间，导致死锁，解决办法是redis2.6.12版本之后，SET命令支持原子操作，可以设置NX选项的同时，设置过期时间。

#### SETNX 示例
```
redis> EXISTS job                # job 不存在
(integer) 0

redis> SETNX job "programmer"    # job 设置成功
(integer) 1

redis> SETNX job "code-farmer"   # 尝试覆盖 job ，失败
(integer) 0

redis> GET job                   # 没有被覆盖
"programmer"
```
#### 

#### SET 示例
```
redis 127.0.0.1:6379> SET not-exists-key "value" NX EX 60
OK      # 键不存在，设置成功，有效时间为60秒

redis 127.0.0.1:6379> GET not-exists-key
"value"

redis 127.0.0.1:6379> SET not-exists-key "new-value" NX EX 60
(nil)   # 键已经存在，设置失败
```